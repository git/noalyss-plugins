begin;

ALTER TABLE importbank.import DROP CONSTRAINT fk_format_bank;
ALTER TABLE importbank.import ADD CONSTRAINT fk_format_bank FOREIGN KEY (format_bank_id) REFERENCES importbank.format_bank(id) ON DELETE SET NULL ON UPDATE SET NULL;
insert into importbank.version (id, v_comment) values (4,'do not cascade when deleting a format of import ');

commit;