begin;
CREATE TABLE importbank.duplicate_record (
	id serial NOT NULL ,
	import_id int8 NOT NULL,
	temp_bank_id2 int8 NULL,
	jr_id int8 NULL,
	temp_bank_id int8 NULL,
	is_similar int4 NOT NULL,
	CONSTRAINT duplicate_record_pkey PRIMARY KEY (id)
);

alter table importbank.duplicate_record add constraint temp_bank_fk1 foreign key (temp_bank_id) references  importbank.temp_bank(id) on delete cascade on update cascade;
alter table importbank.duplicate_record add constraint temp_bank_fk2 foreign key (temp_bank_id2) references  importbank.temp_bank(id) on delete cascade on update cascade;
alter table importbank.duplicate_record add constraint import_id_fk1 foreign key (import_id) references  importbank.import(id) on delete cascade on update cascade;

comment on table importbank.duplicate_record is 'Duplicated or already recorded operation';



insert into importbank.version values (2);
commit;