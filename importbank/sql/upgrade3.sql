begin;

update menu_ref set me_menu='Import Banque' where me_code='IMPORTBANK';

alter table importbank.version add v_comment text;
alter table importbank.version rename "version"  to id;
insert into importbank.version (id, v_comment) values (3,'Change name,improve table version');

commit;