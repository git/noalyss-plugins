begin;

ALTER TABLE importbank.format_bank ADD format_charset int2 NULL DEFAULT 0;
COMMENT ON COLUMN importbank.format_bank.format_charset IS '0 latin1 , 1 unicode';

ALTER TABLE importbank.format_bank ADD CONSTRAINT format_bank_check CHECK (format_charset in (0,1));


insert into importbank.version (id, v_comment) values (5,'Support charset unicode');

commit;