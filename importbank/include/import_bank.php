<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be
global $adecimal, $athousand, $aseparator, $aformat_date, $aheader, $a_status, $cn;
\Noalyss\Dbg::echo_file(__FILE__);
$http = new HttpInput();
$url = array(
    "gDossier" => dossier::id(),
    "plugin_code" => $http->request('plugin_code'),
    "ac" => $http->request("ac"));
/*
 * Step 1/4
 */
if (!isset ($_REQUEST ['sb'])) {
    echo '<div class="content" >';
    $a_format = $cn->get_array(" select id,format_name from importbank.format_bank order by 2");
    require_once('template/import_new.php');
    echo '</div>';
    exit();
}
$http = new HttpInput();
/*
 * Initialize all the fields
 */
$format = new IText('format_name');
$jrn_def = new ISelect('jrn_def');
global $g_user;
$jrn_def->value = $cn->make_array('select jrn_def_id,jrn_def_name from jrn_def where ' . $g_user->get_ledger_sql('FIN', 3) . ' order by jrn_def_name');
$sep_decimal = new ISelect('sep_dec');
$sep_decimal->value = $adecimal;
$sep_decimal->selected = 2;

$sep_thousand = new ISelect('sep_thous');
$sep_thousand->value = $athousand;

$sep_field = new ISelect('sep_field');
$sep_field->value = $aseparator;

$col_valid = new INum('col_valid');

$format_date = new ISelect('format_date');
$format_date->value = $aformat_date;

$file = new IFile('import_file');
$skip = new INum('skip');
$skip->value = 0;
$skip->size = 5;

$nb_col = new INum('nb_col');

$select_charset = new ISelect("p_charset");
$select_charset->value = array(["value" => 0, "label" => _("Latin 1")],
    ["value" => 1, "label" => _("Unicode")
    ]);
$select_charset->selected = 0;
/*
 * Step 2 : show the selected format and upload the file
 */
$sb = $http->request("sb");
if ($sb == 'select_form') {
    $format_id = $http->get("format", "number");
    if ($format_id != '0') {
        $format_bank = new Format_Bank_Sql($cn, $format_id);
        if ($cn->size() == 1) {
            $format->value = $format_bank->format_name;
            $jrn_def->selected = $format_bank->jrn_def_id;
            $sep_field->selected = $format_bank->sep_field;
            $sep_thousand->selected = $format_bank->sep_thousand;
            $sep_decimal->selected = $format_bank->sep_decimal;
            $format_date->selected = $format_bank->format_date;
            $nb_col->value = $format_bank->nb_col;
            $skip->value = $format_bank->skip;
            $select_charset->selected = $format_bank->format_charset;
        } else {
            throw new Exception(_('Nombre de ligne trouvé incorrect'));
        }
    }
    echo '<div class="content" >';
    $sb = 'upload_file';
    require_once('template/show_field.php');
    echo '</div>';
    exit();
}

/*
 * Step 3: upload the file, show it and let change the values of the format
 */
if ($sb == 'upload_file') {
    $format_id = $http->post("format", "number", -1);
    if ($format_id == -1 || isNumber($format_id) == 0) {
        alert(_('Format inconnu'));
        return;
    }
    /**
     * if remove format is asked , delete and return
     */
    if (isset($_POST['remove_format'])) {
        $format_bank = new Format_Bank_Sql($cn, $format_id);
        $format_bank->delete();
        return;
    }
    /*
     * First time or the format is not corrected
     */
    if (!isset($_POST['correct_format'])) {
        $format->value = $http->post('format_name');
        $jrn_def->selected = $http->post('jrn_def');
        $sep_field->selected = $http->post('sep_field');
        $sep_thousand->selected = $http->post('sep_thous');
        $sep_decimal->selected = $http->post('sep_dec');
        $format_date->selected = $http->post('format_date');
        $nb_col->value = $http->post('nb_col');
        $skip->value = $http->post('skip');
        $select_charset->selected = $http->post('p_charset');
        if (trim($_FILES['import_file']['name']) == '') {
            alert(_('Pas de fichier donné'));
            return -1;
        }
        $filename = tempnam($_ENV['TMP'], 'upload_');
        move_uploaded_file($_FILES["import_file"]["tmp_name"], $filename);
        $fbank = fopen($filename, 'r');
        $pos_date = $pos_amount = $pos_lib = $pos_operation_nb = $pos_third = $pos_extra = -1;

        // Load the order of the header
        if ($_POST['format'] != 0) {
            $format_bank = new Format_Bank_Sql($cn, $format_id);
            $pos_date = $format_bank->pos_date;
            $pos_amount = $format_bank->pos_amount;
            $pos_lib = $format_bank->pos_lib;
            $pos_operation_nb = $format_bank->pos_operation_nb;
            $pos_third = $format_bank->pos_third;
            $pos_extra = $format_bank->pos_extra;
            $select_charset->selected = $format_bank->format_charset;
        }
        // Separator for thousand and decimal MUST be
        // different
        if ($sep_decimal->selected == $sep_thousand->selected) {
            alert(_('Les séparateurs décimals et de millier doivent être différents'));

        }
        echo '<div class="content" >';
        $sb = 'confirm';
        require_once('template/confirm_transfer.php');
        echo '</div>';
        exit();
    } else {
        $format->value = $http->post('format_name');
        $jrn_def->selected = $http->post('jrn_def');
        $sep_field->selected = $http->post('sep_field');
        $sep_thousand->selected = $http->post('sep_thous');
        $sep_decimal->selected = $http->post('sep_dec');
        $format_date->selected = $http->post('format_date');
        $nb_col->value = $http->post('nb_col');
        $skip->value = $http->post('skip');
        $select_charset->selected = $http->post("p_charset");

        $filename = $http->post('filename');

        $fbank = fopen($filename, 'r');
        $pos_date = $pos_amount = $pos_lib = $pos_operation_nb = -1;

        $pos_date = $pos_amount = $pos_lib = $pos_operation_nb = $pos_third = $pos_extra = -1;

        // Load the order of the header
        if ($http->post('format') != 0) {
            $format_bank = new Format_Bank_Sql($cn, $http->post('format'));
            $pos_date = $format_bank->pos_date;
            $pos_amount = $format_bank->pos_amount;
            $pos_lib = $format_bank->pos_lib;
            $pos_operation_nb = $format_bank->pos_operation_nb;
            $pos_third = $format_bank->pos_third;
            $pos_extra = $format_bank->pos_extra;

        }
        // Separator for thousand and decimal MUST be
        // different
        if ($sep_decimal->selected == $sep_thousand->selected) {
            alert(_('Les séparateurs décimals et de millier doivent être différents'));
        }
        echo '<div class="content">';
        $sb = 'confirm';
        require_once('template/confirm_transfer.php');
        echo '</div>';
        return;
    }
}
/*
 * Step 4
 * The file is now uploaded, we put in temp and show what has be done, and save the format (or update
 *  if already exist)
 */
if ($sb == 'confirm') {
    $id = $http->post('format', 'number', -1);
    $id = ($id == 0) ? -1 : $id;

    $format->value = $http->post('format_name');
    $jrn_def->selected = $http->post('jrn_def');
    $sep_field->selected = $http->post('sep_field');
    $sep_thousand->selected = $http->post('sep_thous');
    $sep_decimal->selected = $http->post('sep_dec');
    $format_date->selected = $http->post('format_date');
    $nb_col->value = $http->post('nb_col');
    $skip->value = $http->post('skip');
    $select_charset->selected = $http->post("p_charset");

    $format_bank = new Format_Bank_Sql($cn, $id);
    $format_bank->format_name = $http->post('format_name');
    $format_bank->jrn_def_id = $http->post('jrn_def');
    $format_bank->sep_field = $http->post('sep_field');
    $format_bank->sep_thousand = $http->post('sep_thous');
    $format_bank->sep_decimal = $http->post('sep_dec');
    $format_bank->format_date = $http->post('format_date');
    $format_bank->nb_col = $http->post('nb_col');
    $format_bank->skip = $http->post('skip');
    $format_bank->format_charset = $http->post("p_charset");
    /**
     * Check that the destination ledger is well configured and the accounting
     * is properly set and existing
     */
    $jrn_def_id = $jrn_def->selected;

    if ($jrn_def_id == 0) {
        alert(_('Journal financier mal configuré'));
        return;
    }

    // Check if the accounting is correct and exist
    $exist = Import_Bank::check_bank_account($jrn_def_id);

    if ($exist == 0) {
        alert(_('Poste comptable de la fiche banque est incorrect'));
        return;
    }
    /*
     * Verify that we have at least date + amount, and not duplicate
     */
    $check = Import_Bank::is_valid_header($http->post('header'));
    if ($check != '') {
        alert($check);
        /*
         * Back to step 3 !
         */
        $filename = $http->post('filename');

        $fbank = fopen($filename, 'r');
        $pos_date = $pos_amount = $pos_lib = $pos_operation_nb = $pos_third = $pos_extra = -1;
        // Load the order of the header
        if ($http->post('format') != 0) {
            $format_bank = new Format_Bank_Sql($cn, $http->post('format'));
            $pos_date = $format_bank->pos_date;
            $pos_amount = $format_bank->pos_amount;
            $pos_lib = $format_bank->pos_lib;
            $pos_operation_nb = $format_bank->pos_operation_nb;
            $pos_third = $format_bank->pos_third;
            $pos_extra = $format_bank->pos_extra;
        }

        echo '<div class="content">';
        $sb = 'confirm';
        require_once('template/confirm_transfer.php');
        echo '</div>';
        exit();
    }

    /*
     * save the column position for the date, amount,...
     */
    $aHeader = $http->post("header");
    $nb_header = count($aHeader);
    for ($i = 0; $i < $nb_header; $i++) {
        switch ($aHeader[$i]) {
            case 0:
                $format_bank->pos_date = $i;
                break;
            case 1:
                $format_bank->pos_amount = $i;
                break;
            case 2:
                $format_bank->pos_lib = $i;
                break;
            case 3:
                $format_bank->pos_operation_nb = $i;
                break;
            case 4:
                $format_bank->pos_third = $i;
                break;
            case 5:
                $format_bank->pos_extra = $i;
                break;
        }
    }
    $format_bank->save();

    /*
     *read file and save it into importbank.temp_bank
     */
    $fbank = fopen($http->request('filename'), 'r');
    echo '<div class="content">';
    require_once('template/show_transfer.php');
    echo '</div>';
}
