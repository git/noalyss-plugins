<?php
//This file is part of NOALYSS and is under GPL 
//see licence.txt
Noalyss\Dbg::echo_file(__FILE__);
?>
<form id="purge" method="POST">
    <?php
    $http=new \HttpInput();
    $sa=$http->request("sa");
$action="onclick=\"select_checkbox('purge')\"";
echo HtmlInput::button('s','Tout cocher',$action);

$action="onclick=\"unselect_checkbox('purge')\"";
echo HtmlInput::button('u','Tout décocher',$action);
echo HtmlInput::hidden('sa',$sa);
echo HtmlInput::hidden('delete','1');
$action=" onclick=\"return confirm_box('purge','Vous confirmez ?');\"";
echo HtmlInput::submit('delete',_('Supprimer la sélection'),$action);
?>
<table class="table_large">
	<TR>
	<th><?php echo _("num transfert")?></th>
	<Th><?php echo _("Date")?> </Th>
	<Th><?php echo _("Nom format")?></Th>
	<Th><?php echo _("Période")?> </Th>
	<th><?php echo _("Nouveau")?></th>
	<th><?php echo _("Transfèré")?></th>
	<th><?php echo _("Attente")?></th>
	<th><?php echo _("Erreur")?></th>
	<th><?php echo _("Effacé")?></th>
	<Th><?php echo _("Supprimer")?> </Th>
	<th></th>
</TR>
<?php
$nb_row=Database::num_row($ret);
for ($i=0;$i<$nb_row;$i++):

	if ($i%2 == 0 )
		$class='class="even"';
	else
		$class='class="odd"';
	$row=$cn->fetch_array($ret,$i);
$delete=$cn->execute('status',array($row['id'],'D'));
$ndelete=Database::fetch_array($delete,0);

$new=$cn->execute('status',array($row['id'],'N'));
$nnew=Database::fetch_array($new,0);

$error=$cn->execute('status',array($row['id'],'E'));
$nerror=Database::fetch_array($error,0);

$transf=$cn->execute('status',array($row['id'],'T'));
$ntransf=Database::fetch_array($transf,0);

$rec=$cn->execute('status',array($row['id'],'W'));
$nrec=Database::fetch_array($rec,0);


?>
<tr <?php echo $class?>>
<td><?php echo $row['id']?></td>
<td>
<?php echo HtmlInput::hidden('id[]',$row['id']);?>
<?php echo format_date($row['str_date'])?>
</td>
<td>
<?php echo h($row['format_name'])?>
</td>

<td>
<?php echo $row['min_date'] . " &#x2192;  " .$row['max_date'];?>
</td>

<td><?php echo $nnew['count']?></td>
<td><?php echo $ntransf['count']?></td>
<td><?php echo $nrec['count']?></td>

<td><?php echo $nerror['count']?></td>
<td><?php echo $ndelete['count']?></td>

<td>
<?php 
    $select=new ICheckBox('s_del[]',$row['id']);
    $select->set_range("ck_row");
echo $select->input()
?>
</td>


<TD>
<?php
// list
echo HtmlInput::button_anchor('Détail',$link.'&id='.$row['id']);
?>
</tr>

</tr>
<?php 
	endfor;
?>
</table>

<?php
$action="onclick=\"select_checkbox('purge')\"";
echo HtmlInput::button('s',_('Tout cocher'),$action);

$action="onclick=\"unselect_checkbox('purge')\"";
echo HtmlInput::button('u',_('Tout décocher'),$action);
echo HtmlInput::hidden('sa',$_REQUEST['sa']);
echo HtmlInput::hidden('delete','1');
$msg=_("Vous confirmez ?");
$action=" onclick=\"return confirm_box('purge','$msg');\"";
echo HtmlInput::submit('delete',_('Supprimer la sélection'),$action);
if ( $nb_row > 0 ) {
    echo ICheckBox::javascript_set_range("ck_row");
}
?>

</form>
