<?php
//This file is part of NOALYSS and is under GPL 
//see licence.txt
/**
 * @file
 * @brief called from Bank_Item::show_item
 */
?>

<div id="<?php echo $ctl?>">

<?php
echo HtmlInput::title_box('Détail opération',$ctl);
	if ($bi->id=='')
	{
		echo h2('Opération effacée','class="notice"');
		return;
	}
echo "<span style=\"float:right\" class=\"notice\">$msg</span>";
?>
<form method="get" onsubmit="save_bank_info(this);return false;">
<?php
echo HtmlInput::request_to_hidden(array('id','ctl','gDossier','plugin_code','act'));
echo HtmlInput::hidden('p_jrn',$bi->jrn_def_id);
?>
    <div style="position:float;float:left; ">
<table>
<TR>
<TD>Date</TD><td><?php echo $date->input()?></td></tr>
<tr>
	<TD>Journal
	</TD>
	<td><span class="highlight"><?php echo $jrn?></span>
	</td>
</tr>
<tr>
	<TD>Contrepartie
	</TD>
	<td><?php echo $w->input()?><?php echo $w->search()?>
              <?=$str_add_button?>
	</td>
</tr>
<tr>
    <td></td>
    <td class="highlight">
        <span id="e_third"><?php echo h($name)?></span>
    </td>
</tr>   

<tr><td>n° opération </td><td><?php echo h($bi->ref_operation)?></td></tr>
<tr><TD>
	Tiers
    </TD>
    <td>
	<?php echo $third->input()?>
      
    </td>
</tr>
<tr>
	<TD>Montant
	</TD>
	<td><?php echo $amount->input()?>
	</td>
</tr>
<tr>
	<TD>Libelle
	</TD>
	<td><?php echo $libelle->input()?>
	</td>
</tr>
<tr>
	<TD>Autre information
	</TD>
	<td><?php echo $extra->input()?>
	</td>
</tr>
<tr>
	<TD>reconciliation
	</TD>
	<td><?php echo $wConcerned->input();?>
	</td>
</tr>
<?php $style=($bi->status == 'E') ? 'style="color:red;font-weight:bold"' : '';?>

<tr>
	<TD>statut</TD>
	<td <?php echo $style?> ><?php echo $status?></td>
</tr>

</table>
        <?php
    // Show duplicate and already imported operation
    Import_Bank::display_duplicate($bi->id);
    Import_Bank::display_recorded($bi->id);
?>
<?php if ($bi->status != 'D') : ?>
	<?=_("A effacer")?>
	<?php echo $remove->input();?>
<?php else :?>
	<?=_("A ne pas effacer")?>
	<?php echo $recup->input();?>
<?php endif; ?>
        <ul class='aligned-block'>
            <li>
                
<?php echo HtmlInput::submit('save','Sauve');?>
            </li>
            <li>
<?php echo HtmlInput::button_close($ctl)?>
            </li>
        </ul>        
    </div>
</form>
    <div id="div_suggest_<?php echo $ctl;?>" style="position:float;float:left;">
        <h2><?php echo "Suggestion";?></h2>
        <div id="choice_suggest<?php echo $ctl;?>" class="autocomplete_fixed" style="position: static;" >
            
        </div>
    </div>
</div>
