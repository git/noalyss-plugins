<?php

/*
 *   This file is part of NOALYSS.
 *
 *   PhpCompta is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   PhpCompta is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with PhpCompta; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2018) Author Dany De Bontridder <dany@alchimerys.be>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');

/**
 * @file
 * @brief called from Import_Bank::display_recorded
 */
$nb_count=count($a_duplicate);
echo "<h2>"._("Opérations enregistrées similaires")."</h2>";
?>
<table>
<?php
for ($i=0;$i<$nb_count;$i++):
?>
    <tr>
        <td>
            <?php echo $a_duplicate[$i]['jr_date']?>
        </td>
        <td>
            <?php echo HtmlInput::detail_op($a_duplicate[$i]['jr_id'], $a_duplicate[$i]['jr_internal']);?>
        </td>
        <td>
            <?php echo  h($a_duplicate[$i]['jr_comment'])?>
        </td>
        <td>
            
        </td>
    </tr>

<?php endfor; ?>

</table>

