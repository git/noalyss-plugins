<?php

/*
 *   This file is part of NOALYSS.
 *
 *   PhpCompta is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   PhpCompta is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with PhpCompta; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2018) Author Dany De Bontridder <dany@alchimerys.be>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');

/**
 * @file
 * @brief called from Import_Bank::display_duplicate
 */
$nb_count=count($a_duplicate);
global $a_status;
echo "<h2>"._("Imports identiques")."</h2>";
?>
<table>
    
<?php
for ($i=0;$i<$nb_count;$i++):
?>
    <tr>
        
        <td>
            <?=h($a_duplicate[$i]['libelle'])?>
        </td>
        <td>
            <?=$a_status[$a_duplicate[$i]['status']]?>
        </td>
        <td>
            <?php printf (_("Import n°%s du %s"), $a_duplicate[$i]['import_id'],$a_duplicate[$i]['str_date']);?>
        </td>
        <td>
            <?php printf(_("Période  %s au %s"),$a_duplicate[$i]['min_date'],$a_duplicate[$i]['max_date'])?>
        </td>
    </tr>

<?php endfor; ?>

</table>