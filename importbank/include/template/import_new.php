<?php
//This file is part of NOALYSS and is under GPL 
//see licence.txt

// array of bank format

?>
<h2>Etape 1 /4 : choix du format</h2>
<?php
$url = array(
    "gDossier" => dossier::id(),
    "plugin_code" => $_REQUEST['plugin_code'],
    "ac" => $_REQUEST['ac'],
    "sb" => "select_form");
?>
<table class="vert_mtitle">
    <tr>
        <td class="first">
            <a href="?<?php echo http_build_query($url + array("format" => 0)) ?>">
                <?php echo _("Nouveau format"); ?>
            </a>
        </td>
    </tr>
    <?php $nb_format = count($a_format);
    for ($i = 0; $i < $nb_format; $i++): ?>
        <tr>
            <td>
                <a href="?<?php echo http_build_query($url + array("format" => $a_format[$i]['id'])) ?>">
                    <?php echo $a_format[$i]['format_name'] ?>
                </a>
            </td>
        </tr>

    <?php endfor; ?>
</table>

