<?php 
 namespace belgian_tax;
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/*!\file
 * \brief listing intracom
 */
require_once('ext_list_intra.class.php');
echo '<div class="content" >';
\Noalyss\Dbg::echo_file(__FILE__);
 $http=new \HttpInput();
 $year=$http->request("year","string","x");
// verify the year

 if ( isset($_REQUEST['year']) && (trim(strlen($year)) < 4 || isNumber($year) == 0 ||$year < 2000|| $year >2100)) {
     alert(j(_('Année invalide'.' ['.$year.']')));
     echo Ext_List_Intra::choose_periode();
     echo '</div>';
     exit;
 }


// if the periode is not set we have to ask it
if ( ! isset($_REQUEST['decl']) ){
  echo Ext_List_Intra::choose_periode(true);
  echo '</div>';
  exit;
}

$cn=\Dossier::connect();
if (isset($_POST['save'] )) {
  $save=new Ext_List_Intra($cn);
  $save->from_array($_POST);
  $save->insert();
  echo h2info(_('Déclaration sauvée'));
  echo $save->display();

  /**
   *@todo add a div for the button generate, get_xml, create ODS, print...
   */
//   echo '<div style="position:absolute;z-index:14;top:25%;right:30" class="noprint">aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</div>';
  echo '</div>';
  exit;
}

$tva=new Ext_List_Intra($cn);
$periodic=$http->request("periodic","number");

/* by month */
if ($periodic==1) {
  $str_monthly='';
  $str_month=$http->get("bymonth","number");
  $str_year=$http->get("year","number");
  $str_quaterly="";$str_hidden='';$str_submit='';$str_quater='-';
  $tva->blank($str_year,$str_month,1);

}

/* by quater */
if ($periodic == 2) {
  $str_quaterly='';
  $str_month="-";
  $str_quater=$http->get("byquaterly","number");
  $str_year=$http->get("year","number");
  $str_hidden='';$str_submit='';$str_monthly='';
  $tva->blank($str_year,$str_quater,2);
}
/* by year */
if ($periodic == 3) {
  $str_quaterly='';
  $str_month="";
  $str_year=$http->get("year","number");
  $str_quater=$http->get("byquaterly","number");
  $str_byyear='1';
  $str_quater='0';
  $str_hidden='';$str_submit='';$str_monthly='';
  $by_year=true;
  $tva->blank($str_year,$str_quater,3);
}

try {
  $r=$tva->compute();
} catch (Exception $e) {

  echo Ext_List_Intra::choose_periode();
  echo '</div>';
  return;
  }

require_once('form_periode.php');

echo '<form class="print" method="post">';
echo \dossier::hidden();
echo \HtmlInput::extension();
echo \HtmlInput::hidden('start_periode',$tva->start_periode);
echo \HtmlInput::hidden('end_periode',$tva->end_periode);
echo \HtmlInput::hidden('flag_periode',$tva->flag_periode);
echo \HtmlInput::hidden('name',$tva->tva_name);
echo \HtmlInput::hidden('num_tva',$tva->num_tva);
echo \HtmlInput::hidden('adress',$tva->adress);
echo \HtmlInput::hidden('country',$tva->country);
echo \HtmlInput::hidden('periode_dec',$tva->periode_dec);
echo \HtmlInput::hidden('exercice',$tva->exercice);
echo $tva->display_info();
echo $r;
echo $tva->display_declaration_amount();
echo \HtmlInput::submit('save',_('Sauvegarde'));
echo '</form>';
echo '</div>';
