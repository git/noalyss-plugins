/* This file is part of NOALYSS and is under GPL see licence.txt */
function show_declaration(p_type,p_id) {
    try {
	$('detail').innerHTML=loading();
	$('detail').show();
	$('main').hide();
	var gDossier=$('gDossier').value;var code=$('plugin_code').value;
	var queryString='act=dsp_decl&gDossier='+gDossier+'&plugin_code='+code+'&ac='+$('ac').value;
	queryString+='&type='+p_type+'&id='+p_id;
	var action=new Ajax.Request ( 'ajax.php',
				  {
				 method:'get',
				 parameters:queryString,
				 onFailure:error_show_declaration,
				 onSuccess:success_show_declaration
			       }
			       );
    } catch(e) {alert_box('show_declaration '+e.message);}
}
function success_show_declaration(answer) {
    try {
	var xml=answer.responseXML;
	var html=xml.getElementsByTagName('code');
	if ( html.length == 0 ) {var rec=answer.responseText;alert_box ('erreur :'+rec);}
	var code_html=getNodeText(html[0]);
	code_html=unescape_xml(code_html);
	$('detail').innerHTML=code_html;
	code_html.evalScripts();

    } catch(e) {alert_box('success_show_declaration '+e.message);}
}
function error_show_declaration() {
    alert_box('error_show_declaration : ajax not supported');
}
function record_writing(plugin,dossier,p_id) {
    // call ajax to fill with form
    query='gDossier='+dossier+'&plugin_code='+plugin+'&act=rw&p_id='+p_id;

    // add a section
    show_box({id:'record_write',html:loading(),cssclass:'inner_box',style:'position:absolute;top:0;left:0%;margin-top:10%;min-height:80%;margin-left:10%;width:80%;',js_error:null,js_success:success_record_writing,qs:query,fixed:1,callback:'ajax.php'});
}
function remove_form(plugin,dossier,p_id,type) {
    // call ajax to fill with form
    query='gDossier='+dossier+'&plugin_code='+plugin+'&act=rm_form&p_id='+p_id+"&type="+type;

    // add a section
    show_box({id:'remove_form',html:loading(),cssclass:'inner_box',style:'position:absolute;top:0;left:20%;margin-top:10%;width:60%',js_error:null,js_success:success_box,qs:query,callback:'ajax.php'});
}

function success_record_writing(req) {
    try{
	var answer=req.responseXML;
	var a=answer.getElementsByTagName('ctl');
	var html=answer.getElementsByTagName('code');
	if ( a.length == 0 ) {var rec=req.responseText;alert_box ('erreur :'+rec);}
	var name_ctl=a[0].firstChild.nodeValue;
	var code_html=getNodeText(html[0]);

	code_html=unescape_xml(code_html);
	g(name_ctl).innerHTML=code_html;
    }
    catch (e) {
	alert_box("success_box"+e.message);}
    try{
	code_html.evalScripts();}
    catch(e){
	alert_box("answer_box Impossible executer script de la reponse\n"+e.message);}
}

/**
 * Record the generated accounting for purging VAT account
 * @param obj
 * @returns {boolean}
 */
function save_write(obj) {
	var form_date=null;
	for ( let i = 0;i<obj.elements.length;i++) {
		if (obj.elements[i].name == "pdate") { form_date=obj.elements[i];break}
	}
	if ( form_date == null) {
		console.error("VAT no date found");
		return false;
	}
	if ( ! check_date(form_date.value)) {
		form_date.addClassName("input-error");
		return false;
	}

    var query="act=sw&"+obj.serialize();
    var action=new Ajax.Request ( 'ajax.php',
				  {
				      method:'get',
				      parameters:query,
				      onFailure:null,
				      onSuccess:success_save_write
				  });
    return false;
}
function success_save_write(req){

    try{
	var answer=req.responseXML;
	var a=answer.getElementsByTagName('ctl');
	var html=answer.getElementsByTagName('code');
	if ( a.length == 0 ) {var rec=req.responseText;alert_box ('erreur :'+rec);}
	var name_ctl=a[0].firstChild.nodeValue;
	var code_html=getNodeText(html[0]);

	code_html=unescape_xml(code_html);
	g(name_ctl).innerHTML=code_html;
    }
    catch (e) {
	alert_box("success_box"+e.message);}
    try{
	code_html.evalScripts();}
    catch(e){
	alert_box("answer_box Impossible executer script de la reponse\n"+e.message);}
}
function show_addparam(pcode,plugin_code,dossier,tab)
{
	try {
		waiting_box();
		 var action=new Ajax.Request ( 'ajax.php',
				  {
				      method:'get',
				      parameters:"act=add_param&pcode="+pcode+"&gDossier="+dossier+"&plugin_code="+plugin_code+'&tab='+tab,
				      onFailure:null,
				      onSuccess:success_showaddparam
				  });
	}catch (e){
		alert_box(e.message);
	}
}

function success_showaddparam(req) {
  try{
		remove_waiting_box();
		var sx=0;
		if ( window.scrollY)
		{
			sx=window.scrollY+120;
		}
		else
		{
			sx=document.body.scrollTop+120;
		}

		var div_style="top:"+sx+"px;";
		removeDiv("paramadd_id");
		add_div({
			"id":'paramadd_id',
			"drag":"1",
			"cssclass":"inner_box",
			"style":div_style
		});
		var answer=req.responseXML;
		var html=answer.getElementsByTagName('code');
		var code_html=getNodeText(html[0]);
		code_html=unescape_xml(code_html);
		g("paramadd_id").innerHTML=code_html;
	}
	catch (e) {
		alert_box("success_box "+e.message);
	}
	try{
		code_html.evalScripts();
	}
	catch(e){
		alert_box("answer_box Impossible executer script de la reponse\n"+e.message);
	}
}

function tva_show_param(p_div)
{
	try{
		var div=['opin','opout','tvadue','tvaded','divers','lintra','assujetti'];
		for (var r =0;r<div.length;r++ ) {$(div[r]).hide();$("tab_"+div[r]).className="tabs";  }
		$(p_div).show();
		$('tab').value=p_div;
                $("tab_"+p_div).className="tabs_selected";
	} catch(e)
	{
		alert_box(e.message)
	}
}

/**
 * remove a parameter grid for TVA
 * from form_parameter.php
 * @param number p_id table tva_belge.parameter_chld.pi_id
 * 
 */
function delete_param(p_id)
{
    smoke.confirm("Confirmation ?", function (e)
    {
        if (e)
        {
            var param = TvaJSON;
            param['pi_id'] = p_id;
            param['act'] = "delete_param";
            waiting_box();
            new Ajax.Request(
                    "ajax.php",
                    {
                        method: "get",
                        parameters: param,
                        onSuccess: function (req)
                        {
                            var answer = req.responseXML;
                            var html = answer.getElementsByTagName('code');
                            var code_html = getNodeText(html[0]);
                            if (code_html == "OK") {
                                $("tr_" + p_id).hide();
                            } else {
                                var extra = answer.getElementsByTagName('extra');
                                var extra_html = unescape_xml(getNodeText(extra[0]));
                                smoke.alert(extra_html);
                            }
                            remove_waiting_box();

                        }

                    }
            );
        }
    });

}

var TVA_plugin={
	/***
	 * Compute the sum of the ledger
	 */
	compute_sum:function () {
		let sum=0;
		var a_Element=$('vat_operation_form').getElementsByClassName("amount_css");
		var a_CheckBox=$('vat_operation_form').getElementsByClassName("css_checkbox");
		for ( let i=0; i < a_Element.length;i++) {
			var vat_amount=parseFloat(a_Element[i].value);
			if ( isNaN(vat_amount)) vat_amount=0;
			if (a_CheckBox[i].checked ) {
				sum-=vat_amount;
			} else {
				sum+=vat_amount;
			}
			sum = Math.round(sum*100)/100;
		}
		return sum;
	},
	/**
	 * calls compute_sum and display a warning if the amounts are not equals on C and D side
	 */
	check_balance:function () {
		var delta_balance=this.compute_sum();
		$("delta").innerHTML=delta_balance;
		if (delta_balance != 0) {

			$("delta").addClassName("input-error");

		} else {
			$("delta").removeClassName("input-error");
		}

	}
}