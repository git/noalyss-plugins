<?php
namespace belgian_tax;
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
require_once NOALYSS_INCLUDE.'/class/database.class.php';
require_once('ext_tva.class.php');
require_once NOALYSS_INCLUDE.'/lib/ibutton.class.php';
require_once ('ext_list_intra.class.php');
require_once ('ext_list_assujetti.class.php');
require_once NOALYSS_INCLUDE.'/class/acc_ledger.class.php';
require_once NOALYSS_INCLUDE."/lib/http_input.class.php";

extract($_GET, EXTR_SKIP);
global $g_parameter,$cn;

$http=new \HttpInput();
$html='';$extra='';$ctl='';
switch($act) {
    /**
     * display a saved form
     */
case 'dsp_decl':
  /* the hide button */
  $button=new \IButton('hide');
  $button->label=_('Retour');
  $button->javascript="$('detail').hide();$('main').show();";
  if ( $type == 1) {
    /* display the declaration of amount */
    $decl=new Ext_Tva($cn);
    $decl->set_parameter('id',$id);
    $decl->load();
    $r="";
    $r.=$decl->menu();
    $r.=$decl->display();
    $r.=$button->input();
  }
  if ( $type == 3) {
    /* display the declaration for INTRACOM */
    $decl=new Ext_List_Intra($cn);
    $decl->set_parameter('id',$id);
    $decl->load();
    $r="";
    $r.=$button->input();
    $r.=$decl->display();
    $r.=$button->input();
  }
  if ( $type == 2) {
    /* display the declaration of customer */
    $decl=new Ext_List_Assujetti($cn);
    $decl->set_parameter('id',$id);
    $decl->load();
    $r=$button->input();
    $r.=$decl->display();
    $r.=$button->input();
  }

  break;
  /**
   * generate writing
   */
case 'rw':
  require_once NOALYSS_INCLUDE.'/class/acc_ledger.class.php';
  $p_id=$http->request('p_id','number');
  $count=$cn->get_value('select count(*) from tva_belge.declaration_amount where da_id=$1',array($p_id));
  if ( $count == 1 )
    {
      $ctl='record_write';
      $ledger=new \Acc_ledger($cn,0);
      $sel_ledger=$ledger->select_ledger('ODS',1);
      $r=\HtmlInput::title_box('Génération écriture','record_write');
	  if ($sel_ledger != null)
	  {
		  $r.='<form id="vat_operation_form" onsubmit="save_write(this);return false;">';
		  $decl=new Ext_Tva($cn);
		  $decl->set_parameter('id',$p_id);
		  $decl->load();
		  $date=new \IDate('pdate');
                  $r.=\HtmlInput::hidden("p_id",$p_id);
		  $r.="Date :".$date->input().'<br>';
		  $r.="Choix du journal : ".$sel_ledger->input();
          $r.='<span class="text-muted">'._("Opération pour purger la TVA").'</span>';

		  $r.=$decl->propose_form();
		  $r.=\HtmlInput::hidden('mt',microtime(true));
		  $r.=\HtmlInput::extension();
		  $r.=\Dossier::hidden();
          $r.='<ul class="aligned-block">';
          $r.='<li>';
		  $r.=\HtmlInput::submit('save','Sauver','onclick="return confirm(content[47])"');
          $r.='</li>';
          $r.='<li>';
          $r.=\HtmlInput::button_close("record_write");
          $r.='</li>';
          $r.='<li>';
          $r.=\HtmlInput::button(uniqid(),_("Actualise"),'onclick="TVA_plugin.check_balance();return false;"');

          $r.='</li>';
          $r.='</ul>';
		  $r.='</form>';
		} else {
			$r.='<h2 class="error">'._("Aucun journal accessible").'</h2>';
		}
	}
	else
    {
      $ctl='record_write';
      $r=\HtmlInput::title_box(_('Information'),$ctl);
      $r.="<h2 class=\"info\">Désolé cette opération n'existe pas </h2>";
      $r.='<p class="notice">Il se peut que l\'information a été effacée</p>';
      $r.=\HtmlInput::button_close($ctl);
    }
  break;
    /**
     * record in database the operation for purging VAT accounting
     */
case 'sw':
  $ctl='record_write';
  $p_id=$http->request('p_id','number');
  $decl=new Ext_Tva($cn);
  $decl->set_parameter('id',$p_id);
  $decl->load();
  
  ob_start();
  echo \HtmlInput::title_box(_("Enregistrement"), "record_write");
  $http=new \HttpInput();
  $account=$http->get("account",'array',[]);
  $amount=$http->get("amount",'array',[]);
  $lib=$http->get("lib",'array',[]);

  try {
    $array=array();
    $array['p_currency_code']=0;
    $array['p_currency_rate']=1;
    $array['e_date']=$pdate;
    /* give automatically the periode */
    $periode=new \Periode($cn);
    $periode->find_periode($pdate);
    $array['period']=$periode->p_id;
    $idx=0;$nb_item=0;
    foreach ($account as $key=>$value) {
      $i=$key;
      if ( empty($account[$i]) ) continue;
      $array['amount'.$idx]=$amount[$i];
      $array['poste'.$idx]=$account[$i];
      $array['ld'.$idx]=$lib[$i];
      if ( isset($deb[$i])) $array['ck'.$idx]=1;
      $idx++;
      $nb_item++;
    }

   $array['nb_item']=$nb_item;
   $array['e_pj']='';
   $array['e_pj_suggest']='NONE';
   $array['jr_optype']='NOR';
   $array['p_jrn']=$p_jrn;
   $array['mt']=$mt;

   $periodicy = array(1=>_("Mois"),2=>_("Trimestre"));
   $array['desc']=sprintf(_("Déclaration TVA %s %s  %s"),$decl->periode_dec,$periodicy[$decl->periodicity],$decl->exercice);
   $ods=new \Acc_Ledger($cn,$p_jrn);
   $ods->save($array);
   echo h2info("Sauvée : ajoutez le numéro de pièce");
   echo   \HtmlInput::detail_op($ods->jr_id,'détail opération : '.$ods->internal);
   $ods->with_concerned=false;
   echo $ods->confirm($array,true);
   echo \HtmlInput::button_close("record_write");
  } catch(Exception $e) {
    echo alert($e->getMessage());
  }
  $r=ob_get_contents();
  ob_end_clean();
   break;
case 'rm_form':
  switch($type)
    {
    case 'da':
      $sql="delete from tva_belge.declaration_amount where da_id=$1";
      break;
    case 'li':
      $sql="delete from tva_belge.intracomm where i_id=$1";
      break;
    case 'lc':
      $sql="delete from tva_belge.assujetti where a_id=$1";
      break;
    }
  $ctl='remove_form';
  $cn->exec_sql($sql,array($http->request('p_id')));
  $r="";
  $r.=\HtmlInput::title_box(_("Information"), $ctl);
  $r.='<h2 class="notice">'._('Opération effacée').'</h2>';
  $r.=\HtmlInput::button_close($ctl);
  $html=escape_xml($r);
  break;
case 'add_param':
	$ctl="paramadd_id";
	$r=\HtmlInput::title_box("Ajout paramètre",$ctl);
	$r.='<h3>'._('Pour la grille ').$pcode.'</h3>';
	// TVA input
	$text = new \ITva_Popup('tva_id');
	$text->add_label('tva_label');
	$text->js = 'onchange="set_tva_label(this);"';
	$text->with_button(true);

	// Accounting
	$iposte=new \IPoste('paccount');
	$iposte->set_attribute('gDossier',\Dossier::id());
	$iposte->set_attribute('jrn',0);
	$iposte->set_attribute('account','paccount');
	$iposte->set_attribute('label','paccount_label');


	$r.='<form method="POST" id="faddparm"onsubmit="return confirm(\'Vous confirmez ?\');" style="margin-left:15px">';
	$r.=\HtmlInput::request_to_hidden(array("tab","gDossier","plugin_code","ac","pcode"));
	$r.=_(" Code TVA ");
	$r.=$text->input();
	$r.='<span id="tva_label" style="display:block"></span>';
	$r.=" Poste comptable (utilisez le % pour inclure les postes qui en dépendent)";
	$r.=$iposte->input();

	$r.='<span id="paccount_label" style="display:inline"></span>';
	$r.='<span style="display:block"></span>';
	$r.=\HtmlInput::submit("save_addparam","Sauver");
	$r.=\HtmlInput::button_close($ctl);
	$r.='</form>';
	break;
    case 'delete_param':
        /*
         * Remove a parameter from form
         */
        $r="OK";
        $extra="";
        try {
            $pi_id=$http->request("pi_id","number");
            $cn->exec_sql("delete from tva_belge.parameter_chld where pi_id=$1",array($pi_id));
        } catch (Exception $e) {
            record_log(_('delete_param : echec'));
            record_log($e->getMessage());
            record_log($e->getTraceAsString());
            $r="NOK";
            $extra=$e->getMessage();
        }
        break;
default:
  $r=var_export($_REQUEST,true);
}

$html=escape_xml($r);

header('Content-type: text/xml; charset=UTF-8');
echo <<<EOF
<?xml version="1.0" encoding="UTF-8"?>
<data>
<ctl>$ctl</ctl>
<code>$html</code>
<extra>$extra</extra>
</data>
EOF;
?>
