<?php

namespace belgian_tax;

/*
 *   This file is part of NOALYSS.
 *
 *   PhpCompta is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   PhpCompta is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with PhpCompta; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2019) Author Dany De Bontridder <danydb@noalyss.eu>
require_once NOALYSS_INCLUDE.'/class/pdf.class.php';

/**
 * @file
 * @brief 
 */
class TVA_Pdf_Writer extends \Pdf
{

    private $form;

    function __construct(\belgian_tax\Ext_Tva $p_form)
    {
        $this->form=$p_form;
        parent::__construct($p_form->db);
        $this->setDossierInfo(sprintf(_("TVA %s au %s"), format_date($this->form->start_date, 'YYYY-MM-DD', 'DD.MM.YY'),
                        format_date($this->form->end_date, 'YYYY-MM-DD', 'DD.MM.YY')
        ));
        $this->AliasNbPages();

        $this->SetAuthor('NOALYSS');
        $this->SetTitle(_("Déclaration TVA"), 'UTF8');
    }

    function get_form()
    {
        return $this->form;
    }

    function set_form($form)
    {
        $this->form=$form;
    }

    /*     * *
     * Export the form to PDF
     */

    function export()
    {
        $this->AddPage();
        $this->SetFont("DejaVu", "B", "14");
        $this->write_cell(0,12,sprintf(_("TVA %s au %s"), format_date($this->form->start_date, 'YYYY-MM-DD', 'DD.MM.YY'),
                        format_date($this->form->end_date, 'YYYY-MM-DD', 'DD.MM.YY'),0,0,'C'
        ));
        $this->line_new();
        $cn=$this->form->db;

        $form_id=$this->form->get_parameter("form_id");

        $array=$this->cn->get_array("select * from tva_belge.form_detail where form_id=$1 order by fd_order", [$form_id]);

        $nb_array=count($array);
        $this->SetFont("DejaVu", "", "9");
        for ($i=0; $i<$nb_array; $i++)
        {
            $row=$array[$i];

            switch ($row["fd_type"])
            {
                case "h1":
                    $this->setFont("DejaVu", "B", "12");
                    $this->LongLine(190, 8, $row["fd_label"], 1);

                    break;
                case "h2":
                    $this->setFont("DejaVu", "U", "9");
                    $this->LongLine(140, 8, $row["fd_label"]);
                    $attr=$row['fd_tva_code'];
                    if ($attr!=""&&isset($this->form->$attr))
                    {
                        $this->write_cell(40, 5, nbm($this->form->$attr,2), 0, 0, 'R');
                    }
                    break;
                case "t":
                    $this->SetFont("DejaVu", "", "9");
                    $this->write_cell(5, 8, "");

                    $attr=$row['fd_tva_code'];
                    if ($attr!=""&&isset($this->form->$attr))
                    {
                        // set space 20mm
                        $nb_space=preg_match('/\|\>/', $row['fd_label']);

                        for ($e=0; $e<$nb_space; $e++)
                        {
                            $this->write_cell(15, 8, "");
                        }
                        $txt=noalyss_str_replace("|>", "", $row['fd_label']);
                        $pos=145-($nb_space*15);
                        $this->write_cell($pos, 5, $txt);
                        $this->write_cell(40, 5, nbm($this->form->$attr,2), 0, 0, 'R');
                    }
                    else
                    {
                        $this->write_cell(60, 5, $row['fd_label']);
                        switch ($attr)
                        {
                            case "data.year":
                                $this->write_cell(60, 5, $this->form->exercice);

                                break;
                            case "data.periodicity":
                                $per="";
                                if ($this->form->periodicity == 1 ) $per=_("Mensuel");
                                if ($this->form->periodicity == 2 ) $per=sprintf(_(" %s trimestre"),$this->form->periode_dec ) ;
                                $this->write_cell(60, 5, $per);

                                break;
                            case "data.date_from":
                                $this->write_cell(60, 5, format_date($this->form->start_date, 'YYYY-MM-DD', 'DD.MM.YY'));

                                break;
                            case "data.date_to":
                                $this->write_cell(60, 5, format_date($this->form->end_date, 'YYYY-MM-DD', 'DD.MM.YY'));

                                break;
                            case "data.vat_id":
                                $this->write_cell(60, 5, $this->form->num_tva);

                                break;
                            case "data.address":
                                $this->write_cell(60, 5, $this->form->adress);

                                break;
                            case "data.country":
                                $this->write_cell(60, 5, $this->form->country);

                                break;
                            case "data.name":
                                $this->write_cell(100, 5, $this->form->tva_name);
                                break;
                            case "data.date":
                                $this->write_cell(100, 5, format_date($this->form->date_decl));
                            default:
                                break;
                        }
                    }
                    break;
                default:
                    break;
            }
            $this->line_new();
        }
        // compute filename for PDF
        
        $this->output($this->build_file_name(), 'D');
    }
    /***
     * @brief
     * Correct the name of the file , remove forbidden character and
     * add extension and date
     */
    private  function build_file_name()
    {
        

        if (trim(strlen($this->form->tva_name))==0) {
           $file="declaration";
        } else {
            $file=$this->form->tva_name;
        }
        
        $file_name=sprintf("tva-%s-%s-%s.pdf",$this->form->tva_name,$this->form->exercice,$this->form->periode_dec);
        
        $file_name=noalyss_str_replace(";", "", $file_name);
        $file_name=noalyss_str_replace("/", "", $file_name);
        $file_name=noalyss_str_replace(":", "", $file_name);
        $file_name=noalyss_str_replace("*", "", $file_name);
        $file_name=noalyss_str_replace(" ", "_", $file_name);
        $file_name=noalyss_str_replace(">", "", $file_name);
        $file_name=noalyss_str_replace("<", "", $file_name);
        $file_name=noalyss_str_replace("?", "", $file_name);
        $file_name=strtolower($file_name);
        return $file_name;
    }
}
