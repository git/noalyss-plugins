begin;

alter table tva_belge.declaration_amount add form_id int;

update tva_belge.declaration_amount set form_id=1;

create table tva_belge.form(id serial primary key,f_name text not null,f_language char(2)) ;

insert into tva_belge.form (id,f_name,f_language) values (1,'Déclaration TVA','FR');

create table tva_belge.form_detail(id serial primary key, fd_code text not null ,fd_label text not null,fd_type text not null ,fd_tva_code text ,form_id int not null,fd_order int not null);

delete from tva_belge.form_detail;
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T1 ','Cadre I : renseignements généraux','h1','',1,1);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T1.year','Année','t','data.year',1,2);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T1.Month','Mois / trimestre','t','data.periodicity',1,3);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T1.date_from','Date début','t','data.date_from',1,4);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T1.data_to','Date fin','t','data.date_to',1,5);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T1.vat_id ',' N° de tva  ','t','data.vat_id',1,6);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T1.name ',' Nom ','t','data.name',1,7);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T1.adress ',' Adresse ','t','data.address',1,8);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T1.country ',' Pays code postal et localite BE ','t','data.country',1,9);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T2 ',' Cadre II : Opérations à la sortie','h1','',1,10);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T2.00 ',' Opérations soumises à un régime particulier [00]  ','h2','d00',1,11);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T2.B ',' B. Opérations pour lesquelles la T.V.A. est due par le déclarant :','t','',1,12);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T2.B.01 ','|>-au taux de 6% [01]','t','d01',1,13);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T2.B.02 ','|>- au taux de 12% [02]  ','t','d02',1,14);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T2.B.03 ','|>- au taux de 21% [03]  ','t','d03',1,15);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T2.C ','C. Services pour lesquels la T.V.A. étrangère est due par le cocontractant [44]  ','t','d44',1,16);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T2.D ','D. Opérations pour lesquelles la T.V.A. est due par le cocontractant [45]  ','t','d45',1,17);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T2.E ','E. Livraisons intracommunautaires exemptées effectuées en Belgique et ventes ABC [46]  ','t','d46',1,18);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T2.F','F. Autres opérations exemptées et autres opérations effectuées à l''étranger [47]  ','t','d47',1,19);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T2.G','G. Montant des notes de crédit délivrées et des corrections négatives :','t','',1,20);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T2.G.48','|>- relatif aux opérations inscrites en grilles 44 et 46 [48]  ','t','d48',1,21);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T2.G.49','|>- relatif aux autres opérations du cadre II [49]  ','t','d49',1,22);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T3 ',' Cadre III : Opérations à l''entrée','h1','',1,23);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T3.A','A. Montant des opérations à l''entrée compte tenu des notes de crédit reçues et autres corrections :','h2','',1,24);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T3.81','|>- marchandises, matières premières et matières auxiliaires [81]  ','t','d81',1,25);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T3.82','|>- services et biens divers [82]  ','t','d82',1,26);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T3.83','|>- biens d''investissement [83]  ','t','d83',1,27);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T3.B ','B. Montant des notes de crédit reçues et des corrections négatives :','h2','',1,28);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T3.B.84','|> - relatif aux opérations inscrites en grilles 86 et 88 [84]  ','t','d84',1,29);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T3.B.85','|> - relatif aux autres opérations du cadre III [85]  ','t','d85',1,30);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T3.C','C. Acquisitions intracommunautaires effectuées en Belgique et ventes ABC [86]  ','t','d86',1,31);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T3.D','D. Autres opérations à l''entrée pour lesquelles la T.V.A. est due par le déclarant [87]  ','t','d87',1,32);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T3.E','E. Services intracommunautaires avec report de perception [88]  ','t','d88',1,33);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T4','Cadre IV : Taxe due','h1','',1,34);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T4.A','A. T.V.A. relative aux opérations déclarées en :','h2','',1,35);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T4.A.54','|>-grilles 01, 02 et 03 [54]  ','t','d54',1,36);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T4.A.55','|>- grilles 86 et 88 [55]  ','t','d55',1,37);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T4.A.56','|>- grille 87, à l''exception des importations avec report de perception [56]  ','t','d56',1,38);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T4.B.57','B. T.V.A. relative aux importations avec report de perception [57]  ','t','d57',1,39);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T4.C.61 ','C. Diverses régularisations T.V.A. en faveur de l''Etat [61] ','t','d61',1,40);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T4.D','D. T.V.A. à reverser mentionnée sur les notes de crédit reçues [63]  ','t','d63',1,41);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('TXX','Total des grilles 54, 55, 56, 57, 61 et 63 [XX]  ','h2','dxx',1,42);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T5','Cadre V : Taxes déductibles, solde et acompte','h1','',1,43);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T5.A.59','A.T.V.A. déductible [59]  ','t','d59',1,44);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T5.B.62','B.Diverses régularisations T.V.A. en faveur du déclarant [62]  ','t','d62',1,45);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T5.C.64','C.T.V.A. à récupérer mentionnée sur les notes de crédit délivrées [64]  ','t','d64',1,46);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T5.YY','Total des grilles 59, 62 et 64[yy]  ','h2','dyy',1,47);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T6','Cadre VI : Solde*','h1','',1,48);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T6.71','Taxe due à l''Etat : grille xx - grille yy [71]  ','t','d71',1,49);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T6.7','Sommes dues par l''Etat : grille yy - grille xx [72]  ','t','d72',1,50);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T6.COMMENT',' *Une seule des grilles 71 ou 72 peut être remplie','t','',1,51);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T7','Cadre VII : Acompte*','h1','',1,52);
insert into tva_belge.form_detail(fd_code,fd_label,fd_type,fd_tva_code,form_id,fd_order) values ('T7.91','T.V.A. réellement due pour la période du 1er au 20 décembre[91]  ','t','d91',1,53);


insert into tva_belge.version(vdesc) values ('export PDF et CSV');
commit;