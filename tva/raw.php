<?php
namespace belgian_tax;
/*
 *   This file is part of NOALYSS.
 *
 *   PhpCompta is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   PhpCompta is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with PhpCompta; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2002-2019) Author Dany De Bontridder <danydb@noalyss.eu>

/**
 * @file
 * @brief export files CSV or PDF
 */
require_once NOALYSS_INCLUDE."/lib/http_input.class.php";
require_once 'ext_tva.class.php';
require_once 'tva_pdf_writer.class.php';
require_once 'ext_list_assujetti.class.php';
require_once 'ext_list_intra.class.php';


$http=new \HttpInput();;
$operation=$http->get("op");

//-------------------------------------------------------------------------------------------------------------
//Export in PDF for form
//--------------------------------------------------------------------------------------------------------------
if ( $operation == "export_form_pdf" ) {
    
    $decl=new Ext_Tva($cn);
    $id=$decl->set_parameter("id",$http->get('da_id','number'));
    $decl->load();
        
    // Display it thanks TVA_Pdf_Writer
    $pdf=new TVA_Pdf_Writer($decl);
    $pdf->export();
}
//----------------------------------------------------------------------------------------------------------------
// export csv
//----------------------------------------------------------------------------------------------------------------
if ($operation == "exportcsv") {
    $sa=$http->get("sa");
    switch ($sa)
    {
        case "lc":
            // expcsv001 for "assujetti" year : sa == lc
            $decl=new Ext_List_Assujetti($cn);
            $decl->export_csv($http->get("a_id","number"));
            break;
        case "li":
            // expcsv001 for "intra" year : sa == li
            $decl=new Ext_List_Intra($cn);
            $decl->export_csv($http->get("i_id","number"));
            break;
        default:
            record_log("tva.raw.php67");
            record_log(print_r($_GET,true));
    }


}