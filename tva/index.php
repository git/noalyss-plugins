<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/*!\file
 * \brief main file for tva
 */
global $version_plugin;
$version_plugin=SVNINFO;
Extension::check_version(7000);
$http=new HttpInput();

$url='?'.dossier::get().'&plugin_code='.$http->request('plugin_code')."&ac=".$http->request('ac');

$sa=$http->request("sa","string","dec");

$array=array (
	array($url.'&sa=dec',_('Déclaration TVA'),_('Déclaration Trimestriel ou annuel de TVA'),'dec'),
	array($url.'&sa=li',_('Listing intracommunautaire'),_('Listing intracommunautaire trimestriel'),'li'),
	array($url.'&sa=lc',_('Listing Assujetti'),_('Listing des clients assujettis'),'lc'),
	array($url.'&sa=ltva',_('Liste des déclarations TVA'),_('Historique des déclarations TVA'),'ltva'),
	array($url.'&sa=param',_('Paramètrage '),_('Paramètre pour la TVA'),'param')
	);
$a_param=[ 
        "gDossier"=>$http->request("gDossier"),
        'plugin_code'=>$http->request("plugin_code"),
        'ac'=>$http->request("ac")
        ];

$param='var TvaJSON='.json_encode($a_param).";";
echo '<script language="javascript">';
echo $param;
require_once('js_scripts.js');
echo '</script>';


$install=0;
$cn=Dossier::connect();
if ( $cn->exist_schema('tva_belge') == false) {
  require_once __DIR__.'/install_plugin.class.php';
  $install=1;
  $iplugn=new \belgian_tax\Install_Plugin($cn);
  $iplugn->install();
  echo_warning(_("L'extension est installée, pourriez-vous en vérifier le paramètrage ?"));
  $def=5;
}


// check schema
$a=$cn->exist_column('assujetti_chld','ac_periode','tva_belge');
if ( $a == false)
  $cn->exec_sql("alter table tva_belge.assujetti_chld add ac_periode text");

$a=$cn->exist_column('assujetti_chld','exercice','tva_belge');
if ( $a == false)
  $cn->exec_sql("alter table tva_belge.assujetti_chld add exercice text");

$a=$cn->exist_column('declaration_amount','exercice','tva_belge');
if ( $a == false)
  $cn->exec_sql("alter table tva_belge.declaration_amount add exercice text");

$a=$cn->exist_column('intracomm','exercice','tva_belge');
if ( $a == false)
  $cn->exec_sql("alter table tva_belge.intracomm add exercice text");

$a=$cn->exist_column('assujetti','exercice','tva_belge');
if ( $a == false)
  $cn->exec_sql("alter table tva_belge.assujetti add exercice text");
if ( $cn->exist_table("version","tva_belge")==false)
{

  $file=dirname(__FILE__)."/sql/patch2.sql";
    $cn->execute_script($file);
    if ( $install == 0 ) echo_warning(_("Mise à jour du plugin, pourriez-vous en vérifier le paramètrage ?"));
    $def=5;
    }
if ( $cn->get_value("select max(id) from tva_belge.version") == 1 ) {
    $file=dirname(__FILE__)."/sql/patch3.sql";
    $cn->execute_script($file);
    $def=5;
}
echo '<div style="float:right"><a class="mtitle" style="font-size:140%" href="http://wiki.noalyss.eu/doku.php?id=tva" target="_blank">Aide</a>'.
'<span style="font-size:0.8em;color:red;display:inline">vers:SVNINFO</span>'.
'</div>';
// show menu
echo  '<div class="menu2">';
echo ShowItem($array,'H','nav-item ','nav-link',$sa,'nav nav-fill nav-pills nav-level2');
echo '</div>';
?>
<div class="content" style="">
<?php
// include the right file
if ($sa=='dec') {
  require_once('decl_tva.inc.php');
  exit();
}

/* Listing of all */
if ($sa=='ltva') {
  require_once('list_tva.inc.php');
  exit();
}
/* listing intracomm */
if ($sa=='li') {
  require_once('list_intra.inc.php');
  exit();
}
/* listing assujetti */
if ($sa=='lc') {
  require_once('list_assujetti.inc.php');
  exit();
}

/* setting */
if ( $sa=='param') {
  require_once('tva_param.inc.php');
  exit();
}
?>
</div>
