<?php
//This file is part of NOALYSS and is under GPL
//see licence.txt

$http=new \HttpInput();
try
{
    $file=$http->request("act");
    $a_action=['info'];
    if ( in_array ($file,$a_action) )
    {
            require_once 'ajax_'.$file.'.php';
            return;
    } else {
        throw new Exception (_("Action invalide") ) ;

    }
} catch (Exception $e) {
    record_log($e->getMessage());
    record_log($e->getTraceAsString());

}
