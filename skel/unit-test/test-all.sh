#!/bin/bash 
# Test all the files from this folder
# Change the bootstrap.php file with your own setting
# Change the path for phpunit
#
PHPUNIT=~/Program/phpunit

cd `dirname $0`

if [ ! -d coverage ] ; then
    mkdir coverage
fi

rm -f cat test-all.txt

// Set the order of the file
FILE=*Test.class.php 
for i in $FILE; do
	echo Testing $i
	$PHPUNIT --color --debug   --coverage-html coverage  --testdox-text=test-all.txt --verbose --bootstrap  bootstrap.php $i
	
	cat test-all.txt
done

