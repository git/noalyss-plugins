<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/* $Revision$ */

// Copyright (c) 2019 Author Dany De Bontridder dany@alchimerys.be

/**
 *@brief configure the variable here to match your environment and you can
 * run the unit test with PHPUNIT 7.4
 * @code
 *  [...path to phpunit ...] phpunit.phar --bootstrap ./bootstrap.php .
 * @endcode
 */
 // Variable to change
$NOALYSS_HOME="MUST-BE-SET";


// Database id , for example 35 means for the postgresql database dossier35
define ('DATABASE',35);

// various DEFINE to use in your test




// Files require to run the tests, it must be adapted to your need

require_once $NOALYSS_HOME."/include/config.inc.php";
require_once $NOALYSS_HOME."/include/constant.php";
require_once NOALYSS_INCLUDE."/class/database.class.php";
require_once NOALYSS_INCLUDE."/lib/ac_common.php";
require_once NOALYSS_INCLUDE."/lib/zip_extended.class.php";



require '../include/class/read_folder.class.php';
require '../include/class/write_folder.class.php';
require '../include/class/read_import_file.class.php';
require '../include/class/import_folder.class.php';
