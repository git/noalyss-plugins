# Use PHPUnit

At least version 7.3

# Configure

change values in bootstrap.php to suit your own environment, adapt also the phpunit.xml


#Example of test

## Test the class Read_Folder


```bash
~/Program/phpunit  --debug --list-tests --verbose --bootstrap bootstrap.php read_folderTest.class.php
```
or with colors
```
phpunit --color --debug    --verbose --bootstrap bootstrap.php read_folderTest.class.php
```

## List test

```bash
~/Program/phpunit   --list-tests --verbose --bootstrap bootstrap.php read_folderTest.class.php
```

# Debug
You can use \tracedebug 
