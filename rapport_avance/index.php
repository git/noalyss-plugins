<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/**
 * \file
 * \brief main file for tools 
 * 
 */
global $version_plugin;
$version_plugin = SVNINFO;
Extension::check_version(9025);
require_once NOALYSS_INCLUDE.'/lib/noalyss_sql.class.php';
require_once 'rapav_constant.php';
require_once NOALYSS_INCLUDE.'/lib/impress.class.php';
if ($cn->exist_schema('rapport_advanced') == false )
{
	require_once('include/rapav_install.class.php');
	$iplugn = new \rapav\Rapav_Install($cn);
	$iplugn->install();
	\echo_warning(_("L'extension est installée, pourriez-vous en vérifier le paramètrage ?"));
}
if ( $cn->get_value('select max(version_id) from rapport_advanced.version') < $rapav_version )
{
	require_once('include/rapav_install.class.php');
	$iplugn = new \rapav\Rapav_Install($cn);
	$iplugn->upgrade($rapav_version);
}
require_once 'include/rapav_repository.class.php';

/*
 * load javascript
 */
ob_start();
require_once('rapav_javascript.js');
$j = ob_get_contents();
ob_end_clean();
echo create_script($j);

$http=new HttpInput();

$plugin_code=$http->request("plugin_code");
$access_code=$http->request("ac");
$dossier=dossier::id();
echo <<<EOF
<script>
var access_code='$access_code';
var plugin_code='$plugin_code';
var dossier_id='$dossier';
</script>
EOF;

$url="?".http_build_query(["gDossier"=>$dossier,"ac"=>$access_code,"plugin_code"=>$plugin_code]);
        
$array = array(
	array($url . '&sa=fo', _('Formulaire'), _('Création, modification, Paramètre'), 1),
 	array($url . '&sa=de', _('Génération'), _('Génération Déclaration '), 2),
	array($url . '&sa=hi', _('Historique'), _('Historique des déclarations faites'), 3)
);

$sa = $http->request("sa","string","fo");

$def = 0;
switch ($sa)
{
	case 'fo':
		$def = 1;
		break;
	case 'de':
		$def = 2;
		break;
	case 'hi':
		$def = 3;
		break;
}

$cn = Dossier::connect();
// show menu
echo '<div style="float:right" class="nav-level3  nav-pills noprint">'.
     '<a class="nav-link" href="http://wiki.noalyss.eu/doku.php?id=rapav:noalyss" target="_blank" style="display:inline">'.
      _("Aide").
      '</a>'.
'<span style="font-size:0.8em;color:red;display:inline">v:SVNINFO</span>'.
'</div>';
echo '<div class="noprint">';
echo ShowItem($array, 'H', 'nav-item ', 'nav-link ', $def, 'nav nav-pills nav-level3');
echo '</div>';

echo '<div class="content" style="width:80%;margin-left:10%">';
// include the right file

// include the right file
if ($def == 1)
{
	require_once('include/formulaire.inc.php');
	exit();
}

/* Déclaration */
if ($def == 2)
{
	require_once('include/declaration.inc.php');
	exit();
}
/* Historique */
if ($def == 3)
{
	require_once 'include/historique.inc.php';
	exit();
}
?>
