<?php
//This file is part of NOALYSS and is under GPL
//see licence.txt
require_once 'rapav_constant.php';

$act = HtmlInput::default_value_request('act','');
$http=new \HttpInput();
extract($_REQUEST, EXTR_SKIP);
if ( $act=="") {
    die(_('act invalide'));
}
global $cn;
$a_action=explode(',',
        'mod_form,add_row_definition,mod_param,add_param_detail,'.
        'rapav_search_code,save_param_detail,rapav_declaration_display,'.
         'modify_param_detail,'.
         ',modify_rapav_description'.
         ',input_new_param'.
         ',save_new_param'.
         ',form_modify'.
         ',input_delete_param'.
         ',save_definition');
if ( in_array($act,$a_action ) == true )
{
    include 'ajax/ajax_'.$act.'.php';
    exit();
}
switch ($act)
{
    /////////////////////////////////////////////////////////////////////////
    // Delete un formulaire_param_detail
    /////////////////////////////////////////////////////////////////////////
    case 'delete_param_detail':
        $fp_id=$http->request('fp_id',"number");
        $cn->exec_sql("delete from rapport_advanced.formulaire_param_detail "
                . " where fp_id=$1", array($fp_id));
        break;
    /////////////////////////////////////////////////////////////////////
    // Delete a saved declaration (from history)
    /////////////////////////////////////////////////////////////////////
    case 'rapav_declaration_delete':
        $d_id=$http->get("d_id","number");
        $cn->exec_sql("delete from rapport_advanced.declaration where d_id=$1",
                array($d_id));
        break;
    /////////////////////////////////////////////////////////////////////
    // Remove a template
    /////////////////////////////////////////////////////////////////////
    case 'rapav_remove_doc_template':
        require_once 'include/rapav_formulaire.class.php';
        $rapav = new \rapav\Rapav_Formulaire($_GET['f_id']);
        $rapav->remove_doc_template();
        break;
    default:
        if ( DEBUGNOALYSS > 1) var_dump($_GET);
        die ("Aucune action demandée");
}
