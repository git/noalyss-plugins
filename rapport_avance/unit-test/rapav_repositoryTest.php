<?php

/*
 * * Copyright (C) 2022 Dany De Bontridder <dany@alchimerys.be>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 *
 * Author : Dany De Bontridder danydb@noalyss.eu
 * 9/06/23
 */

/**
 * @file
 * @brief noalyss
 */

use PHPUnit\Framework\TestCase;

require 'global.php';

/**
 * @testdox Rapav Repository check the repository
 * @backupGlobals enabled
 * @coversDefaultClass
 */
class Rapav_RepositoryTest extends TestCase
{

    /**
     * @var
     */
    protected $object;
    protected $connection;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test method is executed.
     */
    protected function setUp(): void
    {
        global $g_connection;
        $this->connection = $g_connection;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test method is executed.
     */
    protected function tearDown(): void
    {
        /**
         * example
         * if ( ! is_object($this->object->fiche_def)) return;
         * include_once DIRTEST.'/global.php';
         * $g_connection=Dossier::connect();
         * $sql=new ArrayObject();
         * $sql->append("delete from fiche_detail where f_id in (select f_id from fiche where fd_id =\$1 )");
         * $sql->append("delete from fiche where f_id not in (select f_id from fiche_detail where \$1=\$1)");
         * $sql->append("delete from jnt_fic_attr where fd_id  = \$1 ");
         * $sql->append("delete from fiche_def where fd_id = \$1");
         * foreach ($sql as $s) {
         * $g_connection->exec_sql($s,[$this->object->fiche_def->id]);
         * }
         */
    }

    /**
     * the setUpBeforeClass() template methods is called before the first test of the test case
     *  class is run
     */
    public static function setUpBeforeClass(): void
    {
        //        include 'global.php';
    }

    /**
     *  tearDownAfterClass() template methods is calleafter the last test of the test case class is run,
     *
     */
    static function tearDownAfterClass(): void
    {
	    //if (file_exists('/tmp/web.xml') ) unlink ('/tmp/web.xml');
    }

    /**
     * @testdox Test constructor
     * @covers       \RAPAV\Rapav_Repository::__construct
     * @backupGlobals enabled
     */
    function testConstruct()
    {
        $web_xml = $_ENV['TMP'] . "/web.xml";
        if ( file_exists($web_xml) )        unlink($web_xml);

        $rapav_repository = new \RAPAV\Rapav_Repository();
        $this->assertFileExists($web_xml, $_ENV['TMP'] . "/web.xml not loaded");
        $this->assertGreaterThan(18000, filesize($web_xml), " web.xml truncated");


    }

    /**
     * @testdox Test the cache
     * @covers       \RAPAV\Rapav_Repository::__construct
     * @backupGlobals enabled
     * */
    function testCache()
    {
        $web_xml = $_ENV['TMP'] . "/web.xml";
        $rapav_repository = new \RAPAV\Rapav_Repository();
        $this->assertFileExists($web_xml, $_ENV['TMP'] . "/web.xml not loaded");
        clearstatcache(true, $web_xml);
        $time = filemtime($web_xml);
        sleep(2);
        $rapav_repository2 = new \RAPAV\Rapav_Repository();
        clearstatcache(true, $web_xml);
        $time2 = filemtime($web_xml);
        $this->assertEquals($time, $time2, " web.xml not cached");

        \RAPAV\Rapav_Repository::setTimeCacheSecond(5);

        sleep(10);

        $this->assertEquals(\RAPAV\Rapav_Repository::getTimeCacheSecond(), 5, "cache not set correctly");
        $rapav_repository3 = new \RAPAV\Rapav_Repository();

        clearstatcache(true, $web_xml);
        $time3 = filemtime($web_xml);
        $this->assertNotEquals($time, $time3, " web.xml not refreshed");
    }

    /**
     * @testdox Get the available reports in the repository
     * @covers       \RAPAV\Rapav_Repository::available_report
     * @backupGlobals enabled
     * */
    function testAvailableReport()
    {
        $rapav_repository=new \Rapav\Rapav_Repository();
        $a_report=$rapav_repository->available_report();
        $this->assertNotEquals(0,count($a_report),"No report advanced found");

    }
    /**
     * @testdox find a specific advanced report
     * @covers       \RAPAV\Rapav_Repository::find_report
     * @backupGlobals enabled
     * */
    function testfindReport()
    {
        $rapav_repository=new \Rapav\Rapav_Repository();
        $report=$rapav_repository->find_report("FRBIL01");
        $this->assertTrue(get_class($report)=="SimpleXMLElement","invalid return");

        $report=$rapav_repository->find_report("FRBIL01XXXXX");
        $this->assertTrue(empty($report),"unnkown report must return NULL");

    }



}
