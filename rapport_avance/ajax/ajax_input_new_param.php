<?php
/*
 * * Copyright (C) 2018 Dany De Bontridder <dany@alchimerys.be>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 *
 */

require_once  RAPAV_DIR.'/include/formulaire_param.class.php';
/**
 * @file
 * @brief display a box to enter a new Element
 */
global $cn;
$http=new \HttpInput();

$p_id=$http->request("form_param_id", "number", -1);
$row=new \rapav\formulaire_param_sql($p_id);

$type_row=$cn->make_array("select p_type,p_description from rapport_advanced.type_row order by p_description", 1);
$type_periode=$cn->make_array("select t_id,t_description from rapport_advanced.periode_type order by t_description");
$order=$http->request("p_order", "number", $row->p_order);
echo \HtmlInput::title_box(_("Ajout élément"), "input_new_param_div");
?>

<form method="POST" onsubmit="save_new_param();return false;" id="input_new_param_frm">
    <?php
    echo \HtmlInput::array_to_hidden(['gDossier', 'ac', 'plugin_code', 'form_id', 'form_param_id'], $_REQUEST);
    ?>
    <p>
        <?php
        echo _("Position");
        $p_position=new \INum("p_order");
        $p_position->value=$row->p_order;
        $p_position->value=$order;
        echo $p_position->input();
        ?>
    </p>
    <p>
        <?php
        echo _("Type d'élément");
        $type=new \ISelect("p_type");
        $type->value=$type_row;
        $type->selected=$row->p_type;
        $type->javascript=sprintf('onchange="display_detail(this)"');
        echo $type->input();
        ?>
    </p>
    <p>
        <?php
        echo _("Libelle");
        $p_libelle=new IText("p_libelle");
        $p_libelle->size=80;
        $p_libelle->value=$row->p_libelle;
        echo $p_libelle->input();
        ?>
    </p>
    <p detail="formula">
        <?php
        echo _("Code");
        $code=new \IText("p_code");
        $code->value=$row->p_code;
        echo $code->input();
        ?>
    </p>


    <p detail="formula">
        <?php
        echo _("Type période");
        $periode=new \ISelect("t_id");
        $periode->value=$type_periode;
        $periode->selected=$row->t_id;
        echo $periode->input();
        ?>
    </p>


    <?php
    echo \HtmlInput::submit("save_new_param_s", _("Sauver"));
    echo \HtmlInput::button_close("input_new_param_div");
    ?>
</form>
<script>
    $('p_code').focus();
    display_detail($('p_type'));
</script>
