<?php
/*
 *   This file is part of NOALYSS.
 *
 *   PhpCompta is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   PhpCompta is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with PhpCompta; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2018) Author Dany De Bontridder <dany@alchimerys.be>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');

require_once  RAPAV_DIR.'/include/rapav_formulaire.class.php';
/**
 * @file
 * @param Show a form for modifying form : name , description or files
 */
echo \HtmlInput::title_box(_("Formulaire"), "form_modify_div");
$http=new \HttpInput();
$f_id=$http->get("f_id", "number");
$form=new \rapav\RAPAV_Formulaire($f_id);
?>
<form method="POST" enctype="multipart/form-data" onsubmit="waiting_box();return true;">
    <?php echo \HtmlInput::array_to_hidden(["gDossier", "ac", "plugin_code", "f_id"], $_REQUEST); ?>
    <ul class="select_table" style="padding:3px">
        <li>
            <span style="width:25%">
                <?php
                echo _("Nom");
                ?>
            </span>
            <span>
                <?php
                $f_title=new IText("f_title");
                $f_title->value=$form->f_title;
                $f_title->css_size="60%";
                echo $f_title->input();
                ?>
            </span>
        </li>
        <li>
            <span>

                <?php
                echo _("Description");
                ?>
            </span>
            <span>
                <?php
                $f_description=new IText("f_description");
                $f_description->value=$form->f_description;
                $f_description->css_size="60%";
                echo $f_description->input();
                ?>
            </span>
        </li>
        <li>
            <span>

                <?php
                echo _("Modèle");
                ?>
            </span>
            <span class="span_rapav_template" style="display:inline">
                <?php
                if (noalyss_trim($form->f_filename)!="")
                {
                    echo $form->anchor_document();
                    echo '<span style="font-size:130%">';
                    echo \Icon_Action::trash(uniqid(),
                            sprintf("rapav_remove_doc_template('%s','%s','%s','%s');", $http->request("plugin_code"),
                                    $http->request("ac"), \Dossier::id(), $form->f_id));
                    echo '</span>';

                }
                else
                {
                    $file=new \IFile("rapav_template");
                    echo $file->input();
                }
                ?>
            </span>
            <?php
            if (noalyss_trim($form->f_filename) != "")
            {
                echo    '<span id="rapav_new_file" style="display:none">';
                $file=new \IFile("rapav_template");
                echo $file->input();
                echo '</span>';
            }
            ?>
        </li>
        <li>
            <span>
<?php
echo _("Cochez la case pour effacer ce formulaire");
?>
            </span>
            <span>
<?php
$delete=new ICheckBox('delete');
echo $delete->input();
?>
            </span>
        </li>
    </ul>
    <ul class="aligned-block">
        <li>
<?php echo \HtmlInput::submit('save_form', _("Valider")); ?>
        </li>
        <li>
<?php echo \HtmlInput::button_close("form_modify_div"); ?>
        </li>
    </ul>

</form>
