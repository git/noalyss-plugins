<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/**
 * @file
 * @brief display a declaration from history but you can't modify it
 */
require_once  RAPAV_DIR.'/include/rapav_declaration.class.php';
global $cn;
$http=new \HttpInput();
$decl = new rapav\Rapav_Declaration();
$decl->d_id = $http->get('d_id','number');
echo \HtmlInput::button_action("Retour","$('declaration_list_div').show(); $('declaration_display_div').hide();",'rt'.$decl->d_id,'smallbutton');
$decl->load();
echo '<ul class="aligned-block">';
echo '<li>';

// export in CSV
$ref=HtmlInput::array_to_string(array('gDossier','plugin_code','d_id'),$_GET,'extension.raw.php?');
$ref.="&amp;act=export_decla_csv";

// export in PDF
$ref_pdf = HtmlInput::array_to_string(array('gDossier', 'plugin_code', 'd_id'), $_REQUEST, 'extension.raw.php?');
$ref_pdf.="&amp;act=export_pdf";
echo '<ul class="aligned-block noprint">';
echo '<li>';
echo HtmlInput::button_anchor(_("Export CSV"), $ref, 'export_id', "", 'button');
echo     '</li>';
    

echo '<li>'.
        HtmlInput::button_anchor(_("Export PDF"), $ref_pdf, 'export_id', "", 'button').
'</li>';
if ($decl->d_filename != '' && $decl->d_step == 0) {
    echo '<li>'.
        $decl->anchor_document().
        '</li>';
}
if ($decl->d_pdf_filename != '' && $decl->d_step == 0) {
    echo '<li>'.
        $decl->anchor_pdf().
        '</li>';
}
echo '</ul>';

$decl->display();
echo '<ul class="aligned-block noprint">';
echo '<li>';
$ref=HtmlInput::array_to_string(array('gDossier','plugin_code','d_id'),$_GET,'extension.raw.php?');
$ref.="&amp;act=export_decla_csv";
echo HtmlInput::button_anchor(_("Export CSV"), $ref, 'export_id', "", 'button');
echo     '</li>';
    

// export in PDF
echo '<li>'.
        HtmlInput::button_anchor(_("Export PDF"), $ref_pdf, 'export_id', "", 'button').
'</li>';
if ($decl->d_filename != '' && $decl->d_step == 0) {
    echo '<li>'.
        $decl->anchor_document().
        '</li>';
}
if ($decl->d_pdf_filename != '' && $decl->d_step == 0) {
    echo '<li>'.
        $decl->anchor_pdf().
        '</li>';
}
echo '</ul>';
echo '<hr>';

echo HtmlInput::button_action("Retour","$('declaration_list_div').show(); $('declaration_display_div').hide();",'rt','smallbutton');

?>
