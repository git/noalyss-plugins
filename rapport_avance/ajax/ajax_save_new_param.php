<?php

/*
 * * Copyright (C) 2018 Dany De Bontridder <dany@alchimerys.be>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once  RAPAV_DIR.'/include/formulaire_param.class.php';

/**
 * @file
 * @brief in the box "Add Element" save the new element
 */
$http=new \HttpInput();
try
{
    $form_id=$http->post("form_id", "number");
    $code=$http->post("p_code", "string", NULL);
    $label=$http->post("p_libelle");
    $order=$http->post("p_order", "number", 0);
    $type_row=$http->post("p_type", "number", NULL);
    $periode_type=$http->post("t_id", "number", NULL);
    $form_param_id=$http->post('form_param_id', 'number', -1);
}
catch (Exception $e)
{
    \record_log($e->getMessage());
    \record_log($e->getTraceAsString());
    \header('Content-Type: application/json;charset=utf-8');
    $answer=["status"=>"NOK", "message"=>$e->getMessage()];
    echo \json_encode($answer, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_NUMERIC_CHECK);
    return;
}

try
{
    // $form_param_id == -1 if new row otherwise a real value
    $row_sql=new \rapav\formulaire_param_sql($form_param_id);
    $old_type=$row_sql->p_type;
    $row_sql->p_code=$code;
    $row_sql->p_libelle=$label;
    $row_sql->p_type=$type_row;
    $row_sql->p_order=$order;
    $row_sql->t_id=$periode_type;
    $row_sql->f_id=$form_id;
    $new_row=new \rapav\Formulaire_Param($row_sql);

    // If type of row change then clear Formulaire_Param_Detail
    if ($old_type != $row_sql->p_type ) {
        $new_row->clear_detail();
    }

    $new_row->save();

}
catch (Exception $ex)
{
    \record_log($e->getMessage());
    \record_log($e->getTraceAsString());
    \header('Content-Type: application/json;charset=utf-8');
    $answer=["status"=>"NOK", "message"=>$e->getMessage()];
    echo \json_encode($answer, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_NUMERIC_CHECK);
    return;
}

ob_start();
$new_row->obj->load();
$new_row->input();
$new_row->add_button();
$content=ob_get_contents();
ob_clean();

$answer=["status"=>"OK", "message"=>"", "content"=>$content,"order"=>$new_row->obj->p_order,"form_param_id"=>$new_row->obj->p_id];
\header('Content-Type: application/json;charset=utf-8');
echo \json_encode($answer, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_NUMERIC_CHECK);
