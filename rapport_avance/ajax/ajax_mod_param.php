<?php
//This file is part of NOALYSS and is under GPL
//see licence.txt

/**
 * @file
 * @brief Montre le résultat et permet de changer les paramètrages d'un formulaire
 *  uniquement pour ceux ayant un champs de calcul (formule, code tva+poste comptable + totaux intermédiare
 */
require_once  RAPAV_DIR.'/include/rapav_formulaire.class.php';
$http=new \HttpInput();
$f_id=$http->request("f_id","number");
$form = new \rapav\RAPAV_formulaire($f_id);
$form->reorder();
$form->load_definition();
$form->echo_formulaire();
$form->input_parameter();
?>
