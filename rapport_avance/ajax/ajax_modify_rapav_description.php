<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// Copyright 2015 Author Dany De Bontridder danydb@aevalys.eu

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');
global $cn;
require_once NOALYSS_INCLUDE."/lib/inplace_edit.class.php";

$http=new \HttpInput();
$input=$http->request("input");
$action=$http->request("ieaction", "string", "display");
$d_id=$http->request("d_id", "number");
switch ($action)
{
    case "ok":
        $value=$http->request("value");
        $in_desc=\Inplace_Edit::build($input);
        $in_desc->set_callback("ajax.php");
        $in_desc->add_json_param("ac", $http->request("ac"));
        $in_desc->add_json_param("plugin_code", $http->request("plugin_code"));
        $in_desc->add_json_param("gDossier", \Dossier::id());
        $in_desc->add_json_param("act", "modify_rapav_description");
        $in_desc->add_json_param("d_id", $d_id);
        $in_desc->set_value($value);
        $cn->exec_sql("update rapport_advanced.declaration set d_description=$1 where d_id=$2",
                [$value,$d_id]);
        echo $in_desc->value();
        
        break;
    case "display":
        $in_desc=\Inplace_Edit::build($input);
        $in_desc->set_callback("ajax.php");
        $in_desc->add_json_param("ac", $http->request("ac"));
        $in_desc->add_json_param("plugin_code", $http->request("plugin_code"));
        $in_desc->add_json_param("gDossier", \Dossier::id());
        $in_desc->add_json_param("act", "modify_rapav_description");
        $in_desc->add_json_param("d_id", $d_id);
        echo $in_desc->ajax_input();
        break;
    case "cancel":
        $in_desc=\Inplace_Edit::build($input);
        $in_desc->set_callback("ajax.php");
        $in_desc->add_json_param("ac", $http->request("ac"));
        $in_desc->add_json_param("plugin_code", $http->request("plugin_code"));
        $in_desc->add_json_param("gDossier", \Dossier::id());
        $in_desc->add_json_param("act", "modify_rapav_description");
        $in_desc->add_json_param("d_id", $d_id);
        echo $in_desc->value();
        break;

    default:
        break;
}
