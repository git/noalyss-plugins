begin;

alter table rapport_advanced.declaration add d_pdf_filename text;
alter table rapport_advanced.declaration add d_pdf_size  text;
alter table rapport_advanced.declaration add d_pdf_lob  oid;

comment on column rapport_advanced.declaration.d_pdf_filename is 'PDF file name ';
comment on column rapport_advanced.declaration.d_pdf_size is 'PDF file size ';
comment on column rapport_advanced.declaration.d_pdf_lob is 'PDF file binary ';

insert into rapport_advanced.version(version_id,version_note) values (7,'PDF');
commit;