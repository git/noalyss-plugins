begin;
update rapport_advanced.formulaire_param_detail set fp_formula =replace(fp_formula,'%s]','%-s]') where fp_formula like '%s]%' ; 
update rapport_advanced.formulaire_param_detail set fp_formula =replace(fp_formula,'%S]','%-S]') where fp_formula like '%S]%' ;
update rapport_advanced.formulaire_param_detail set fp_formula =replace(fp_formula,'%d]','%-d]') where fp_formula like '%d]%' ;
update rapport_advanced.formulaire_param_detail set fp_formula =replace(fp_formula,'%c]','%-c]') where fp_formula like '%c]%' ;
insert into rapport_advanced.version(version_id,version_note) values (10,'Adapt to Noalyss 9');
commit;