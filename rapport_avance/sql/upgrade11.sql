begin;

alter table rapport_advanced.listing_param drop constraint listing_param_tva_id_fkey;
alter table rapport_advanced.listing_param add constraint listing_param_tva_id_fkey foreign key (tva_id) references tva_rate(tva_id) on update cascade ;
insert into rapport_advanced.version(version_id,version_note) values (11,'VAT code Mantis#1327');
commit;