begin;

ALTER TABLE rapport_advanced.restore_formulaire_param drop CONSTRAINT restore_formulaire_param_pkey cascade;
ALTER TABLE rapport_advanced.restore_formulaire_param_detail drop CONSTRAINT restore_formulaire_param_detail_pkey cascade;
delete from rapport_advanced.restore_formulaire_param_detail;
delete from rapport_advanced.restore_formulaire_param;

ALTER TABLE rapport_advanced.restore_formulaire_param_detail add f_id int8;
comment on column rapport_advanced.restore_formulaire_param_detail.f_id is 'id of the import';

insert into rapport_advanced.version(version_id,version_note) values (8,'Fix bug for importing');
commit;