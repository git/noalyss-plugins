<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/**
 * @file
 * @brief liste des déclarations + modification, création...
 *
 */
require_once 'rapav_formulaire.class.php';
require_once 'rapav_report_repository.class.php';
require_once 'formulaire_param.class.php';
$http=new \HttpInput();
//--------------------------------------------------------------------------------------------------------------------
// Save or delete an advanced report
//--------------------------------------------------------------------------------------------------------------------
if (isset($_POST['save_form']))
{
    $f_id=$http->post("f_id", "number");
    if (isset($_POST['delete']))
    {
        $cn->exec_sql('delete from rapport_advanced.formulaire where f_id=$1', array($_POST['f_id']));
    }
    else
    {
        try
        {
            $form=new \rapav\RAPAV_Formulaire($f_id);
            $form->f_title=$http->post("f_title");
            $form->f_description=$http->post("f_description");
            $form->update();
            if (isset($_FILES['rapav_template'])&&$_FILES['rapav_template']['name']!="")
            {
                \rapav\RAPAV_Formulaire::load_file($form);
            }
            // return to the right place
            $form->reorder();
        }
        catch (\Exception $e)
        {
            \alert($e->getMessage());
        }
        $form->load_definition();
        echo '<div id="form_mod_div">';

        $form->echo_formulaire();
        $form->input_parameter();
        echo '</div>';

        return;
    }
}
//--------------------------------------------------------------------------------------------------------------------
// Add a new report
//--------------------------------------------------------------------------------------------------------------------

if (isset($_POST['add_form']))
{
    try
    {
        $form=new rapav\RAPAV_Formulaire();
        $form->f_title=$http->post("titre");
        $form->f_description=$http->post("description");
        $form->insert();
        $form->load();
        $form->load_definition();
    }
    catch (\Exception $e)
    {
        \alert($e->getMessage());
    }
    echo '<div id="form_mod_div">';

    $form->echo_formulaire();
    $form->input_parameter();
    echo '</div>';
    return;
}

//--------------------------------------------------------------------------------------------------------------------
// Upload and install an advanced report from a file
//--------------------------------------------------------------------------------------------------------------------

if (isset($_POST['restore_form']))
{
    // Sauver fichier
    if ($_FILES['form']['name']==""||$_FILES["form"]["error"]!=0)
    {
        echo _("Fichier non chargé");
    }
    rapav\RAPAV_Formulaire::import($_FILES['form']['tmp_name']);
}
//--------------------------------------------------------------------------------------------------------------------
// Download and installed advanced report
//--------------------------------------------------------------------------------------------------------------------
if (isset($_POST['download_report']))
{
    $a_package_code=$http->post("package");
    if ( ! empty($a_package_code)) {
        // For each AR
        foreach ($a_package_code as $item) {
            $package=\rapav\Rapav_Report_Repository::build($item);
            // install it
            $package->install();

        }

    }

}

rapav\RAPAV_formulaire::listing();
?>
