<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Copyright Author Dany De Bontridder danydb@aevalys.eu 9/06/23

/*! 
 * \file
 * \brief Manage the Advanced Report from the repository
 */
namespace rapav;

class Rapav_Repository extends \Package_Repository
{



    /**
     * @brief display available Advanced Report to download
     * @return HTML string
     */
    public function available_report() {
        $a_advanced_repo=$this->getContent()->xpath("//contrib/rapav/report");
        return $a_advanced_repo;

    }



    public function display_form()
    {
        $a_package=$this->available_report();
        if (empty ($a_package)) {
            return '<p class="notice">'._("Aucun rapport trouvé").'</p>';
        }
        ob_start();
        include 'template/rapav_repository-display_form.php';
        return ob_get_clean();
    }

    /**
     * @brief find the XML from web.xml matching the code or NULL
     * @param $p_code
     * @return void
     */
    public function find_report($p_code)
    {
        if ( $this->content == NULL ) {
            throw new Exception(_("Problème réseau"),10);
        }
        $a_plugin=$this->content->xpath('//contrib/rapav/report');
        $nb_plugin=count($a_plugin);
        for ($i=0; $i<$nb_plugin; $i++)
        {
            if (trim($a_plugin[$i]->id)==$p_code)
            {
                return $a_plugin[$i];
            }
        }
        return NULL;
    }

}

