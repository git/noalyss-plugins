<?php

namespace rapav;
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Copyright Author Dany De Bontridder danydb@aevalys.eu 10/06/23
/*! 
 * \file
 * \brief Manage the report from contrib->rapav->report from web.xml file
 *
 */

class Rapav_Report_Repository extends \Package_Plugin
{
    private $xml;

    public function __construct($name, $description, $full_path, $xml)
    {
        parent::__construct($name, $description, $full_path);
        $this->xml = $xml;

    }

    /**
     * @return mixed
     */
    public function get_xml()
    {
        return $this->xml;
    }

    /**
     * @param mixed $xml
     */
    public function set_xml($xml)
    {
        $this->xml = $xml;
        return $this;
    }


    function download()
    {
        $full=$this->get_path()."/".$this->get_file();
        $file = file_get_contents(NOALYSS_PACKAGE_REPOSITORY."/".$full);
        $filename=$_ENV['TMP'].DIRECTORY_SEPARATOR.$this->get_file();
        $fh_file=fopen($filename,"w+");
        fwrite($fh_file, $file);
        fclose($fh_file);
        return $filename;
    }
    /**
     * @brief install a new advanced report in the DB
     * @return int|void
     * @throws \Exception
     */
    function install()
    {
        try {

           $file = $this->download();

            // file = NOALYSS_HOME/tmp/file
            $work_dir = tempnam($_ENV['TMP'], "rapav");
            unlink($work_dir);
            mkdir($work_dir);


            $zip = new \Zip_Extended();
            $zip->open($file);
            $zip->extractTo($work_dir);

            // Normally there is a bin and a optional second file
            if (!file_exists($work_dir . DIRECTORY_SEPARATOR . $this->xml->id . ".bin")) {
                throw new \Exception(_('Pas de fichier bin'));
            }
            // insert that file in the DATABASE
            $form_sql = RAPAV_Formulaire::import($work_dir . DIRECTORY_SEPARATOR .$this->xml->id . ".bin");
            $template_file = noalyss_trim($this->xml->template_file);
            // Import the template if any
            if (!empty($template_file) && file_exists($work_dir . DIRECTORY_SEPARATOR . $template_file) ) {

                $cn = \Dossier::connect();
                $cn->start();
                $oid = $cn->lo_import($work_dir . DIRECTORY_SEPARATOR . $template_file);
                // check if the lob is in the database
                if ($oid == false) {
                    $cn->rollback();
                    return 1;
                }
                $form_sql->f_lob = $oid;
                $form_sql->f_filename = Rapav_Formulaire::normalize_filename( $template_file);
                $form_sql->f_mimetype = mime_content_type($work_dir . DIRECTORY_SEPARATOR . $template_file);
                $form_sql->f_size = filesize($work_dir . DIRECTORY_SEPARATOR . $template_file);
                $form_sql->update();
                $cn->commit();
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    static function build($p_report_code)
    {
        // open web.xml and find the report
        $repo = new Rapav_Repository();
        $report_repository = $repo->find_report($p_report_code);
        if ($report_repository === false) throw new \Exception(_("RRR40 : report not found"));

        $report_repository = new Rapav_Report_Repository($report_repository->name,
            $report_repository->description,
            $report_repository->path,
            $report_repository);
        return $report_repository;

    }


}
