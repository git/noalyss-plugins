<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/**
 * @file
 * @brief Déclaration
 *
 */
require_once 'rapav_declaration.class.php';
global $cn;
$http = new \HttpInput();

/*
 * Save the date (update them)
 */
if (isset($_POST['save']))
{
    // export in CSV
    $ref_csv = HtmlInput::array_to_string(array('gDossier', 'plugin_code', 'd_id'), $_REQUEST, 'extension.raw.php?');
    $ref_csv.="&amp;act=export_decla_csv";
    
    // export in PDF
    $ref_pdf = HtmlInput::array_to_string(array('gDossier', 'plugin_code', 'd_id'), $_REQUEST, 'extension.raw.php?');
    $ref_pdf.="&amp;act=export_pdf";
    
    $decl = new \rapav\Rapav_Declaration();
    $decl->d_description =$http->post('p_description','string','');
    $decl->d_id = $http->post('d_id',"number");
    $decl->load();
    $decl->to_keep = 'Y';
    $decl->f_id = $http->post("p_form");
    $decl->save();
    if ($decl->d_step == 0)
    {
        $decl->generate_document();
    } else
    {
        // get empty lob
        $decl->d_filename = null;
        $decl->d_size = null;
        $decl->d_mimetype = null;
        $decl->d_lob = null;
        $decl->d_pdf_filename = null;
        $decl->d_pdf_size = null;
        $decl->d_pdf_lob = null;
        $decl->update();
    }
    $text_save=  '<p class="notice">' . _(' Sauvé ') . date('d-m-Y H:i') . '</p>';
    
    echo $text_save;
    echo '<ul class="aligned-block noprint">';
    echo '<li>'.
          HtmlInput::button_anchor(_("Export CSV"), $ref_csv, 'export_id', "", 'button').
          '</li>';
    
    echo '<li>'.
          HtmlInput::button_anchor(_("Export PDF"), $ref_pdf, 'export_id', "", 'button').
          '</li>';
    
    if ($decl->d_filename != '' && $decl->d_step == 0) {
        echo '<li>'.
            $decl->anchor_document().
            '</li>';
    }
    if ($decl->d_pdf_filename != '' && $decl->d_step == 0) {
        echo '<li>'.
            $decl->anchor_pdf().
            '</li>';
    }
    echo '</ul>';
    $decl->display();
    echo $text_save;

    $ref_csv = HtmlInput::array_to_string(array('gDossier', 'plugin_code', 'd_id'), $_REQUEST, 'extension.raw.php?');
    $ref_csv.="&amp;act=export_decla_csv";
    echo '<ul class="aligned-block noprint">';
    echo '<li>'.
          HtmlInput::button_anchor(_("Export CSV"), $ref_csv, 'export_id', "", 'button').
          '</li>';
    

    if ($decl->d_filename != '' && $decl->d_step == 0) {
        echo '<li>'.
            $decl->anchor_document().
            '</li>';
    }
    if ($decl->d_pdf_filename != '' && $decl->d_step == 0) {
        echo '<li>'.
            $decl->anchor_pdf().
            '</li>';
    }
        echo '<li>'.
          HtmlInput::button_anchor(_("Export PDF"), $ref_pdf, 'export_id', "", 'button').
          '</li>';
    
    echo '</ul>';
    return;
    
}
/*
 * compute and propose to modify and save
 */
if (isset($_GET['compute']))
{
    $decl    = new rapav\Rapav_Declaration();
    $p_start = $http->get("p_start");
    $p_end   = $http->get("p_end");
    $p_form  =$http->get("p_form");
    $p_step  =$http->get("p_step","number");
    $decl->d_description = $http->get('p_description');
    
    
    if (isDate($p_start) == 0 || isDate($p_end) == 0)
    {
        alert(_('Date invalide'));
    } else
    {
        $decl->compute($p_form, $p_start, $p_end, $p_step);
        echo '<form class="print" method="POST">';
        echo \HtmlInput::hidden('p_form', $p_form);
        $decl->display();
        echo '<p>';
        echo \HtmlInput::submit('save', _('Sauver'));
        echo '</p>';
        echo '</form>';
        return;
    }
}
/*
 * For rapport
 */
$date_start = new IDate('p_start');
$date_end = new IDate('p_end');
$hidden = HtmlInput::array_to_hidden(array('gDossier', 'ac', 'plugin_code', 'sa'), $_GET);
$select = new ISelect('p_form');
$select->value = $cn->make_array('select f_id,f_title from rapport_advanced.formulaire order by 2');
$description = new ITextArea('p_description');
$description->heigh = 2;
$description->style = ' class="itextarea" style="margin:0"';

$description->width = 80;

$istep = new ISelect('p_step');
$istep->value = array(
    array('label' => 'Aucun', 'value' => 0),
    array('label' => '7 jours', 'value' => 1),
    array('label' => '14 jours', 'value' => 2),
    array('label' => '1 mois', 'value' => 3),
    array('label' => '2 mois', 'value' => 4),
    array('label' => '3 mois', 'value' => 5),
    array('label' => '6 mois', 'value' => 6),
    array('label' => '1 an', 'value' => 7)
);

?>
<div id="id_rapport_div" style="display: block">
    <form id="declaration_form_id" method="GET" onsubmit="return validate()">
                    <?php echo $hidden ?>
        <input type="hidden" name="form" value="rapport">
        <table style="min-width: 40%">
            <tr>
                <td>
                    Formulaire
                </td>
                <td>
<?php echo $select->input() ?>
                </td>
            </tr>
            <tr>
                <td><?php echo _("Description")?></td><td> <?php echo $description->input() ?></td>
            </tr>
            <tr>
                <td>
                    <?php echo _("Date de début")?>
                </td>
                <td>
                    <?php echo $date_start->input() ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo _("Date de fin")?>
                </td>
                <td>
                    <?php echo $date_end->input() ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo _("Etape")?>
                </td>
                <td>
<?php echo $istep->input() ?>
                </td>
            </tr>
        </table>
        </p>
<?php echo HtmlInput::submit('compute', _('Générer')) ?>
    </form>
</div>

<script charset="UTF8" lang="javascript">
    function validate() {
        if (check_date_id('<?php echo $date_start->id ?>') == false) {
            smoke.alert('<?php echo _("Date incorrecte")?>');
            $('<?php echo $date_start->id ?>').style.borderColor = 'red';
            $('<?php echo $date_start->id ?>').style.borderWidth = 2;
            return false;
        }
        if (check_date_id('<?php echo $date_end->id ?>') == false) {
            smoke.alert('<?php echo _("Date incorrecte")?>');
            $('<?php echo $date_end->id ?>').style.borderColor = 'red';
            $('<?php echo $date_end->id ?>').style.borderWidth = 2;
            return false;
        }
        waiting_box();
        return true;
    }
</script>