<?php

namespace rapav;

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/**
 * @file
 * @brief factory display the definition and parameters of a form
 *  mother class of Formulaire_Title1| Formulaire_Title2| Formulaire_Title3| Formulaire_Formula
 *
 */
require_once 'rapport_avance_sql.class.php';
require_once 'formulaire_param_detail.class.php';

/**
 * @brief manage the table rapport_avance.formulaire_param
 */
class Formulaire_Param
{

    var $obj; //!< rapav\Formulaire_Param_SQL object

    function __construct(formulaire_param_sql $e)
    {
        $this->obj=$e;
    }

    /**
     * Factory, create an object following the $form->p_type,
     * @param Formulaire_Param_Sql $form
     * @return Formulaire_Title1| Formulaire_Title2| Formulaire_Title3| Formulaire_Formula
     */
    static function factory(Formulaire_Param_Sql $form)
    {
        switch ($form->p_type)
        {
            case 1:
                return new Formulaire_Title1($form);
            case 2:
                return new Formulaire_Title2($form);
            case 6:
                return new Formulaire_Title3($form);
            case 3:
                return new Formulaire_Formula($form);
            case 7:
                return new Formulaire_Text($form);
            case 8:
                return new Formulaire_Notice($form);
            case 9:
                return new Formulaire_Child($form);
            case 10:
                return new formulaire_Title0($form);
            default:
                throw new \Exception("Unknow type","RAPAV_FP1");
        }
    }

    /**
     * @brief load all the row from formulaire_param_detail, children of formulaire_param
     *  return an array of objects Formulaire_Param_Detail
     * @param type $p_id
     */
    static function load_all($p_id)
    {
        global $cn;
        $a_value=$cn->get_array("select fp_id,type_detail from rapport_advanced.formulaire_param_detail where p_id=$1",
                array($p_id));
        return $a_value;
    }

    function modify_button()
    {
        echo \Icon_Action::modify(uniqid(),
                sprintf("input_modify_param('%s','%s','%s')", $this->obj->f_id, $this->obj->p_id, $this->obj->p_order
        ));
    }

    function delete_button()
    {
        echo \Icon_Action::trash(uniqid(),
                sprintf("input_delete_param('%s')", $this->obj->p_id
        ));
    }
    /**
     * Display a button for adding new parameter
     */
    function add_button()
    {
        echo '<span style="margin-left:-50px">';
        echo \HtmlInput::button_action(_("Ajout élément"),
                sprintf("input_new_param('%s','%s')", $this->obj->f_id, $this->obj->p_order+1
                ), uniqid());
        echo '</span>';
    }

    function input()
    {
        $a=Formulaire_Param::factory($this->obj);
        $a->input();
    }

    function display()
    {
        $a=Formulaire_Param::factory($this->obj);
        $a->display();
    }

    function get_code()
    {
        return $this->obj->p_code;
    }

    function get_text()
    {
        return $this->obj->p_libelle;
    }

    function get_order()
    {
        return $this->obj->p_order;
    }

    function get_id()
    {
        return $this->obj->p_id;
    }

    function get_periode_type()
    {
        return $this->obj->t_id;
    }

    function get_periode_type_text()
    {
        $array=[1=>_("D'après date"), 2=>"N", 3=>"N-1", 4=>"N-2", 5=>"N-3", 6=>_("Début début exercice N")];
        return $array[$this->obj->t_id];
    }

    function save()
    {
        global $cn;

        if ($this->obj->p_type==3||$this->obj->p_type==9)
        {
            // remove forbidden char from p_code
            $this->obj->p_code=preg_replace('/[^[:alnum:]^_]/', '', $this->obj->p_code);

            // check if code is uniq in the form
            if (
                    $cn->get_value("select count(*) from rapport_advanced.formulaire_param where f_id=$1 and p_id <> $2 and p_code=$3",
                            [$this->obj->f_id, $this->obj->p_id, $this->obj->p_code])>0||trim($this->obj->p_code)=="")
            {
                $this->obj->p_code=RAPAV_Formulaire::generate_code();
            }
        }
        else
        {
            $cn->exec_sql("delete from rapport_advanced.formulaire_param_detail where p_id=$1",[$this->obj->p_id]);
            $this->obj->p_code=NULL;
        }
        do {
            $nb_order=$cn->get_value("select count(*) from rapport_advanced.formulaire_param where f_id=$1 and p_order=$2 and p_id <> $3",
                    [$this->obj->f_id,$this->obj->p_order,$this->obj->p_id]);
            if ( $nb_order > 0 ) {
                $this->obj->p_order++;
            } else {
                break;
            }
        } while(true);
        
        // update or insert the row
        if ($this->obj->p_id==-1)
            $this->obj->insert();
        else
            $this->obj->update();
    }
    /**
     * @brief Clear the table Formulaire_Param_Detail , useful when the type of row change , example formula to text 
     * or List_of_account to formula , ...
     * @global type $cn
     */
    function clear_detail()
    {
        global $cn;
        $cn->exec_sql("delete from rapport_advanced.formulaire_param_detail where p_id=$1",[$this->obj->p_id]);
    }

}

/**
 * @brief display title level 1
 */
class formulaire_title1 extends Formulaire_Param
{

    function display()
    {
        echo '<h2 class="title">', h($this->get_text()).'</h2>';
    }

    function input()
    {
        echo '<h2 class="title">',
        sprintf(" [%s] ", $this->get_order()),
        h($this->get_text());
        $this->modify_button();
        $this->delete_button();
        echo '</h2>';
    }

}
/**
 * @brief display title level 1
 */
class formulaire_title0 extends Formulaire_Param
{

    function display()
    {
        echo '<h1 class="title" Style="font-size:2.2rem;border:solid 1px black;padding:1.4rem">', h($this->get_text()).'</h1>';
    }

    function input()
    {
        echo '<h1 class="title" Style="font-size:2.2rem;border:solid 1px black;padding:1.4rem">',
        sprintf(" [%s] ", $this->get_order()),
        h($this->get_text());
        $this->modify_button();
        $this->delete_button();
        echo '</h1>';
    }

}
/**
 * @brief display text
 */
class formulaire_text extends Formulaire_Param
{

    function display()
    {
        echo '<p>'.$this->get_text().'<p>';
    }

    function input()
    {
        echo '<p>'.
        sprintf(" [%s] ", $this->get_order()).
        $this->get_text();
        $this->modify_button();
        $this->delete_button();
        echo '</p>';
    }

}

/**
 * @brief display title level 1
 */
class formulaire_notice extends Formulaire_Param
{

    function display()
    {
        echo \span($this->get_text(), ' class="notice" ');
    }

    function input()
    {
        echo '<span class="notice" style="display:block">';
        printf("[%s] %s ", $this->get_order(), $this->get_text());
        $this->modify_button();
        $this->delete_button();
        echo '</span>';
    }

}

/**
 * @brief display title level 2
 */
class formulaire_title2 extends Formulaire_Param
{

    function display()
    {
        echo '<h3  class="title">'.h($this->get_text()).'</h3>';
    }

    function input()
    {
        echo '<h3 class="title">'.
        sprintf(" [%s] ", $this->get_order()).
        h($this->get_text());
        $this->modify_button();
        $this->delete_button();
        echo '</h3>';
    }

}

/**
 * @brief display title level 3
 */
class formulaire_title3 extends Formulaire_Param
{

    function display()
    {
        echo "<h4>".h($this->get_text())."</h4>";
    }

    function input()
    {
        echo "<h4 class=\"title\">".
        sprintf(" [%s] ", $this->get_order()).
        $this->get_text();
        $this->modify_button();
        $this->delete_button();
        echo "</h4>";
    }

}

/**
 * @brief display the formula : depending of the type of formula, a factory is used and an object RAPAV_Formula, RAPAV_Account_TVA
 * or RAPAV_compute will be used for the display of the details
 */
class Formulaire_Formula extends Formulaire_Param
{

    function __construct(formulaire_param_sql $e)
    {
        $this->obj=$e;
        $this->id=$e->p_id;
        $this->parametre=Formulaire_Param::load_all($this->id);
    }

    function display()
    {
        echo $this->get_text();
    }

    /**
     * @brief return an object following the key type_detail of the array passed in parameter
     *
     * @param type $p_index
     * @return RAPAV_Formula| RAPAV_Account_Tva| RAPAV_Compute
     */
    function make_object($p_index)
    {
        $elt=$this->parametre[$p_index]['type_detail'];
        switch ($elt)
        {
            case '1':
                return new RAPAV_Formula($this->parametre[$p_index]['fp_id']);
                break;
            case '2':
                return new RAPAV_Account_Tva($this->parametre[$p_index]['fp_id']);
                break;
            case '3':
                return new RAPAV_Compute($this->parametre[$p_index]['fp_id']);
                break;
            case '4':
                return new RAPAV_Account($this->parametre[$p_index]['fp_id']);
                break;
            case '5':
                return new RAPAV_Reconcile($this->parametre[$p_index]['fp_id']);
                break;
        }
    }

    /**
     * @brief input value
     */
    function input()
    {
        $http=new \HttpInput();


        echo '<p class="highlight">';
        printf(_("[%s]"), $this->get_order());

        echo $this->get_text()."(".$this->obj->p_code.")";
        $this->modify_button();
        $this->delete_button();
        echo '</p>';
        echo '<p>';
        echo _("période");
        echo "  ";
        echo $this->get_periode_type_text();

        echo '</p>';
        echo \HtmlInput::hidden('p_id[]', $this->obj->p_id);
        $max=count($this->parametre);
        echo \HtmlInput::hidden("count_".$this->id, $max);
        echo '<p>';
        echo '<table id="table_'.$this->id.'">';
        for ($i=0; $i<$max; $i++)
        {
            $formula=$this->make_object($i);

            echo '<tr id="tr_'.$formula->fp_id.'">';
            echo '<td>';
            echo $formula->display_row();
            echo '</td>';
            echo $formula->button_delete();
            echo $formula->button_modify();
            echo '</td>';
            echo '</tr>';
        }
        if ($max==0)
            echo '<tr></tr>';
        echo "</table>";

        echo \Icon_Action::icon_add(uniqid(), sprintf("add_param_detail('%s');", $this->id), 'class="icon"');



        echo '</p>';
    }

}

class Formulaire_Child extends Formulaire_Formula
{

    function __construct(formulaire_param_sql $e)
    {
        parent::__construct($e);
    }

    function make_object($p_index)
    {
        $elt=$this->parametre[$p_index]['type_detail'];
        return new RAPAV_Account($this->parametre[$p_index]['fp_id']);
    }

    /**
     * @brief input value
     */
    function input()
    {
        echo '<p class="highlight">';
        printf(" [%s] %s (%s) ", $this->get_order(),
                $this->get_text(),
                $this->get_code());
        echo \hi(_("Liste de poste"));
        $this->modify_button();
        $this->delete_button();
         echo '</p>';
        
        echo '<p>';
        echo _("période");
        echo "  ";
        echo $this->get_periode_type_text();

        echo '</p>';
        echo \HtmlInput::hidden('p_id[]', $this->obj->p_id);
        $max=count($this->parametre);
        echo \HtmlInput::hidden("count_".$this->id, $max);
        echo '<p>';
        echo '<table id="table_'.$this->id.'">';
        for ($i=0; $i<$max; $i++)
        {
            $formula=$this->make_object($i);

            echo '<tr id="tr_'.$formula->fp_id.'">';
            echo '<td>';
            echo $formula->display_row();
            echo '</td>';
            echo $formula->button_delete();
            echo $formula->button_modify();
            echo '</tr>';
        }
        if ($max==0)
            echo '<tr></tr>';
        echo "</table>";
        echo \Icon_Action::icon_add("add_row".$this->id, sprintf("add_param_detail('%s');", $this->id), 'class="icon"');
        if ($max>0)
        {
            echo "<script> 
                    $('add_row".$this->id."').hide();
                    </script>
                    ";
        }
        echo '</p>';
    }

}

?>
