<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/**
 * @file
 * @brief display list of formulaire + button
 *
 */
$http=new HttpInput();
$access_code=$http->request("ac");
$plugin_code=$http->request("plugin_code");
$dossier_id=Dossier::id();

?>
<div id="form_list_div">
<?php echo _("Recherche")." ".
        HtmlInput::filter_table("form_listing_1", "0", 0);?>
<table class="result" id="form_listing_1">
<?php
	for ($i=0;$i<count($alist);$i++):
            $class=($i%2==0)?'class="odd"':'class="even"';
?>
	<tr <?php echo $class; ?>>
		<td >
                    <?php

                    $js=sprintf("rapav_form_param('%s','%s','%s','%s')",
                            $plugin_code,
                            $access_code,
                            $dossier_id,
                            $alist[$i]['f_id']);


                    ?>
                    <a class="mtitle" onclick="<?php echo $js;?>" style="display:block;padding:2px;width: 100%;margin:0px;height:100%">
                    <span class="highlight"><?php echo h($alist[$i]['f_title'])?></span>
			( <?php echo h($alist[$i]['f_description'])?> )
                    </a>
		</td>

	</tr>

<?php endfor; ?>
</table>
	<?php

    /*******************************************************************************************************************
     * Dialog box for adding a new report
     ******************************************************************************************************************/
echo HtmlInput::button("add_form_bt",
                   _("Ajout d'un formulaire"),
        'onclick="$(\'add_form_div\').show();"');
echo '<div id="add_form_div" style="display:none;position:fixed;top:10rem;" class="inner_box" >';

echo HtmlInput::title_box( _("Nouveau formulaire"),"add_form_div", "hide");
\Noalyss\Dbg::echo_file(__FILE__.":".__LINE__);
?>
    <p class="notice">
        <?=_("Créer, télécharger ou importer un formulaire.")?>
    </p>
    <p>
        <?=_("Les formulaires sont disponibles dans le Package Repository ou depuis les contributions")?>
        <a href="https://wiki.noalyss.eu/doku.php?id=menu:rapav#contributions"  target="_blank"><?=_("Contributions sur le wiki")?></a>
    </p>
    <p>
        <a href="https://wiki.noalyss.eu/doku.php?id=menu:rapav" target="_blank">
        <?=_("Vous pouvez les modifier afin de les améliorer ou de vous en inspirer pour en créer de nouveaux")?>
        </a>

    </p>
    <?php
$tab_blank=new \Html_Tab("new_rapav",_("Nouveau"));

// Create a new Advanced Report from scratch
//--------------------------------------------
$name=new IText("titre");
$description=new IText("description");
$description->size=80;
ob_start();
echo '<form method="POST">';
echo h2(_('Formulaire vide'));
echo '<table>';
echo tr(td(_("Titre ")).td($name->input()));
echo tr(td(_('Description')).td($description->input()));
echo '</table>';
echo '<ul class="aligned-block">';
echo '<li>';
echo HtmlInput::submit ("add_form",_("Sauver"));
echo '</li>';
echo '<li>';
echo HtmlInput::button_hide("add_form_div");
echo '</li>';
echo '</ul>';
echo '</form>';
$tab_blank->set_content(ob_get_contents());
ob_end_clean();


// Upload a new Advanced Report from a file
//--------------------------------------------
$tab_file=new \Html_Tab("file_rapav",_("Fichier"));
ob_start();
echo '<form enctype="multipart/form-data"  method="POST"> ';
echo h2(_('Fichier formulaire'));
$file=new IFile('form');
echo $file->input();
echo '<ul class="aligned-block">';
echo '<li>';
echo HtmlInput::submit ("restore_form",_("Sauver"));
echo '</li>';
echo '<li>';
echo HtmlInput::button_hide("add_form_div");
echo '</li>';
echo '</ul>';
echo '</form>';
$tab_file->set_content(ob_get_contents());
ob_end_clean();

// Download a new Advanced Report from the repository
//----------------------------------------------------
$tab_repo=new \Html_Tab("repo_rapav",_("Téléchargement"));
$repo=new \Rapav\Rapav_Repository();
$str =  $repo->display_form();
ob_start();
    echo '<form enctype="multipart/form-data"  method="POST"> ';
    echo h2(_('Rapports disponibles '));
    echo $str;
    echo '<ul class="aligned-block">';
    echo '<li>';
    echo HtmlInput::submit ("download_report",_("Sauver"));
    echo '</li>';
    echo '<li>';
    echo HtmlInput::button_hide("add_form_div");
    echo '</li>';
    echo '</ul>';
    echo '</form>';

$tab_repo->set_content(ob_get_clean());


$output=new \Output_Html_Tab();
$output->add($tab_repo);
$output->add($tab_blank);
$output->add($tab_file);
$output->output();
echo create_script($output->build_js("repo_rapav"));
?>
</div>

</div>
<div id="form_mod_div">

</div>
