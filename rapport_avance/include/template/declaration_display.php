<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be
require_once NOALYSS_INCLUDE."/lib/inplace_edit.class.php";
/**
 * @file
 * @brief display content of a declaration
 *
 */
$http=new \HttpInput();
?>
<h1><?php echo $this->d_title?></h1>
<h2> Du <?php echo $this->d_start?> au <?php echo $this->d_end?></h2>
<?php
$desc=new \ITextarea("p_description");
$desc->id="p_description";
$desc->value=$this->d_description;

$in_desc=new \Inplace_Edit($desc);
$in_desc->set_callback("ajax.php");
$in_desc->add_json_param("ac", $http->request("ac"));
$in_desc->add_json_param("plugin_code", $http->request("plugin_code"));
$in_desc->add_json_param("gDossier", \Dossier::id());
$in_desc->add_json_param("act", "modify_rapav_description");
$in_desc->add_json_param("d_id", $this->d_id);

echo $in_desc->input();
?>
  
<?php 
	if ( empty($array) ) { echo 'Aucune donnée'; exit();}

	// Create infobulle
	echo HtmlInput::hidden('d_id',$this->d_id);
echo "<p></p>";
for ($i=0;$i<count($array);$i++):
	$row=$array[$i];
if ($this->d_step <> 0 ) $per= $row['dr_start']." : ".$row['dr_end'];
$per=(isset($per))?$per:"";
switch($row['dr_type'])
{
	case 10:
            // Title 0
		echo '<h1  class="title" Style="font-size:2.2rem;border:solid 1px black;padding:1.4rem">'.$row['dr_libelle'].'</h1>';
		break;
	case 2:
            // Title 2
		echo '<h3 class="title" Style="font-size:1.4rem;">'.$row['dr_libelle'].'</h3>';
		break;
	case 1:
            // Title 1
		echo '<h2  class="title">'.$row['dr_libelle'].'</h2>';
		break;
	case 6:
            // Title 3
		echo '<h4  class="title">'.$row['dr_libelle'].'</h4>';
		break;
	case 3:
            // Formula
		$input=new INum('amount[]',round($row['dr_amount'],2));
		$input->size=15;
		$input->prec=2;
		echo HtmlInput::hidden('code[]',$row['dr_id']);
		echo 
                     '<span style="display:inline-block;width:70%">'.
                      $row['dr_libelle'].
                      " </span> ".
                '<span style="display:inline-block;width:29%">'.
                        $input->input()."  $per ".
                      " </span> ";
                
                     
		break;
	case 7:
                // text
		echo '<p>'.$row['dr_libelle'].'</p>';
		break;
	case 8:
                // NOtice
		echo '<p class="notice" > '.$row['dr_libelle'].'</p>';
		break;
    case 9:
            // Account children
            if ( $row['dr_amount'] == 0) {
                break ;
            }
            $input=new INum('amount[]',round($row['dr_amount'],2));
            $input->size=15;
            $input->prec=2;
		    echo HtmlInput::hidden('code[]',$row['dr_id']);
                echo 
                     '<span style="display:inline-block;width:70%;margin-left:1rem">'.
                       $row['dr_account'].'/'.
                       $row['dr_libelle'].
                      " </span> ".
                '<span style="display:inline-block;">'.
                        $input->input()."  $per ".
                      " </span> ";
                
		break;

}

endfor;
?>
