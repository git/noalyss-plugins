<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Copyright Author Dany De Bontridder danydb@aevalys.eu 10/06/23
/*! 
 * \file
 * \brief display the available advanced report from repository
 *
 */
\Noalyss\Dbg::echo_file(__FILE__);
$icheckbox = new ICheckBox('package[]');
$icheckbox->set_range("package_range");
echo HtmlInput::filter_table("package_tbl", '0,1,2', 0);
?>
    <table class="result sortable" id="package_tbl">
        <tr>
            <th><?= _("Code") ?></th>
            <th><?= _("Nom") ?></th>
            <th><?= _("Description") ?></th>
            <th class="sorttable_nosort"></th>

        </tr>


        <?php
        $i=0;
        foreach ($a_package as $package) :

            ?>
            <tr class="<?=($i++%2)?'odd':'even'?>">
                <td>
                    <?= $package->id ?>
                    <?php
                    if (DEBUGNOALYSS > 1) echo \Noalyss\Dbg::hidden_info("package", $package);
                    ?>
                </td>
                <td>
                    <?= h($package->name) ?>

                </td>
                <td>

                    <?= $package->description ?>
                </td>

                <td class="nosorttable">

                    <?php
                    $icheckbox->value = $package->id;
                    echo $icheckbox->input();
                    ?>
                </td>
            </tr>
        <?php
        endforeach;
        ?>
    </table>
<?php
echo \ICheckBox::javascript_set_range("package_range");
?>