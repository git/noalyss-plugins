<?php

namespace rapav;

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/**
 * @file
 * @brief Manage les formulaires
 *
 */
require_once 'rapport_avance_sql.class.php';
require_once 'formulaire_param.class.php';
require_once NOALYSS_INCLUDE."/lib/output_html_tab.class.php";
require_once NOALYSS_INCLUDE."/lib/html_tab.class.php";

class RAPAV_Formulaire extends Formulaire_Sql
{

    var $f_id; //!< primary key for rapav.formulaire
    var $definition; //!< array of Formulaire_Param_Sql (rapav.formulaire_param)

    function __construct($f_id=-1)
    {
        $this->f_id=$f_id;
        $this->definition=array(); //!< Formulaire_Param_Sql array
        parent::__construct($f_id);
    }

    /**
     *  show a list of all existing declaration
     * @global type $cn database connection
     */
    static function listing()
    {
        global $cn;
        $alist=$cn->get_array("select f_id,f_title,f_description
			from rapport_advanced.formulaire order by 2");
        require 'template/formulaire_listing.php';
    }

    /**
     * Anchor to the template
     * @return html anchor string
     */
    function anchor_document()
    {
        $url=\HtmlInput::request_to_string(array('gDossier', 'ac', 'plugin_code'));
        $url='extension.raw.php'.$url.'&amp;act=export_definition_modele&amp;id='.$this->f_id;
        return \HtmlInput::anchor($this->f_filename, $url);
    }

    /**
     * Get data from database, from the table rapport_advanced.formulaire_param
     */
    function load_definition()
    {
        $f=new Formulaire_Param_Sql();
        $ret=$f->seek(" where f_id=".sql_string($this->f_id)." order by p_order ");
        $max=\Database::num_row($ret);

        for ($i=0; $i<$max; $i++)
        {
            $o=new Formulaire_Param_Sql();
            $o=$f->next($ret, $i);
            $this->definition[$i]=clone $o;
        }
    }
    /**
     * Input the form
     * @deprecated
     */
    function input_formulaire()
    {
        throw new Exception("OBSOLETE ".__DIR__);
        $this->load();
        require_once 'template/formulaire_titre.php';
    }

    /**
     * input the definition
     * @deprecated
     */
    function input_definition()
    {
        throw new Exception("OBSOLETE ".__DIR__);
        $max=count($this->definition);
        global $cn;

        require 'template/formulaire_definition.php';
    }

    /**
     * save the definition
     * $p_array contains
     *   - f_id id of the formulaire
     *   - f_title title of the formulaire
     *   - f_description description of the formulaire
     *   - p_id array of the row in formulaire_param
     *   - p_code array of the row in formulaire_param
     *   - p_libelle array of the row in formulaire_param
     *   - p_type array of the row in formulaire_param
     *   - t_id array of the row in formulaire_param
     *   - p_order array of the row in formulaire_param
     *
     */
    static function save_definition($p_array)
    {
        self::verify_definition($p_array);
        self::update_definition($p_array);
        return;
    }

    /**
     * @brief Check data and change them if needed
     * @global database connection $cn
     * @param array $p_array normally $_POST
     */
    static function verify_definition(&$p_array)
    {
        global $cn;
        $count_code=count($p_array['p_code']);
        for ($i=0; $i<$count_code; $i++)
        {
            $c=$cn->get_value('select count(*) from rapport_advanced.formulaire_param
				where p_code=$1 and p_id <> $2 and f_id=$3', array($p_array['p_code'][$i], $p_array['p_id'][$i], $p_array['f_id']));

            if ($c>0)
            {
                $p_array['p_code'][$i]=RAPAV_Formulaire::generate_code();
            }
        }

        for ($i=0; $i<$count_code; $i++)
        {
            for ($e=0; $e<$count_code; $e++)
            {
                if ($p_array['p_code'][$i]==$p_array['p_code'][$e]&&$i!=$e)
                {
                    $p_array['p_code'][$e]=RAPAV_Formulaire::generate_code();
                }
            }
        }
    }
    function update()
    {
        if ( trim ($this->f_title == "")) {
                throw new \Exception(_("Titre ne peut être vide"));
        }
        parent::update();
    }
    function insert()
    {
        if ( trim ($this->f_title == "")) {
                throw new \Exception(_("Titre ne peut être vide"));
        }

        parent::insert();
    }
    /**
     *
     * @see save_definition
     * @param type $p_array
     */
    static function update_definition($p_array)
    {
        global $cn;
        $rapav=new RAPAV_Formulaire($p_array['f_id']);
        // save into table formulaire
        $rapav->f_title=$p_array['f_title'];
        $rapav->f_description=$p_array['f_description'];
        $rapav->update();
        $nb_line=count($p_array['p_id']);
        for ($i=0; $i<$nb_line; $i++)
        {
            $form_param=new formulaire_param_sql($p_array['p_id'][$i]);
            $form_param->p_code=(trim($p_array['p_code'][$i])!="")?$p_array['p_code'][$i]:RAPAV_Formulaire::generate_code();
            // remove space from p_code
            $form_param->p_code=noalyss_str_replace(' ', "", $form_param->p_code);

            $form_param->p_libelle=trim($p_array['p_libelle'][$i]);
            if ($form_param->p_libelle=='')
                continue;
            $form_param->p_type=$p_array['p_type'][$i];
            $form_param->p_order=(isNumber($p_array['p_order'][$i])==0)?($i+1)*10:$p_array['p_order'][$i];
            $form_param->t_id=$p_array['t_id'][$i];
            $form_param->f_id=$p_array['f_id'];
            // update or insert the row
            if ($p_array['p_id'][$i]==-1)
                $form_param->insert();
            else
                $form_param->update();
        }

        // delete checked rows
        if (isset($p_array["del_row"]))
        {
            for ($i=0; $i<count($p_array['del_row']); $i++)
            {
                if (isNumber($p_array['del_row'][$i])==1&&$p_array['del_row'][$i]!=-1)
                {
                    $cn->exec_sql('delete from rapport_advanced.formulaire_param where p_id=$1',
                            array($p_array['del_row'][$i]));
                }
            }
        }
        // Reorder
        //
                $order_param=new formulaire_param_sql();
        $ret=$order_param->seek(' where f_id=$1 order by p_order', array($p_array['f_id']));
        $nb_rows=Database::num_row($ret);
        for ($i=0; $i<$nb_rows; $i++)
        {
            $param=$order_param->next($ret, $i);
            $param->p_order=($i+1)*10;
            $param->update();
        }
        self::load_file($rapav);
    }
    /**
     * @brief reorder
     */
    function reorder()
    {
        $order_param=new formulaire_param_sql();
        $ret=$order_param->seek(' where f_id=$1 order by p_order', array($this->f_id));
        $nb_rows=\Database::num_row($ret);
        for ($i=0; $i<$nb_rows; $i++)
        {
            $param=$order_param->next($ret, $i);
            $param->p_order=($i+1)*10;
            $param->update();
        }
    }

    /**
     * Load the template
     * @global $cn
     * @param \rapav\RAPAV_Formulaire $p_rapav
     * @return int
     */
    static function load_file(RAPAV_Formulaire $p_rapav)
    {
        global $cn;
        // nothing to save
        if (sizeof($_FILES)==0)
            return;

        // Start Transaction
        $cn->start();
        $name=$_FILES['rapav_template']['name'];
        $new_name=tempnam($_ENV['TMP'], 'rapav_template');
        // check if a file is submitted
        if (strlen($_FILES['rapav_template']['tmp_name'])!=0)
        {
            // upload the file and move it to temp directory
            if (move_uploaded_file($_FILES['rapav_template']['tmp_name'], $new_name))
            {
                $oid=$cn->lo_import($new_name);
                // check if the lob is in the database
                if ($oid==false)
                {
                    $cn->rollback();
                    return 1;
                }
            }
            // the upload in the database is successfull
            $p_rapav->f_lob=$oid;
            $p_rapav->f_filename=Rapav_Formulaire::normalize_filename($_FILES['rapav_template']['name']);
            $p_rapav->f_mimetype=$_FILES['rapav_template']['type'];
            $p_rapav->f_size=$_FILES['rapav_template']['size'];

            // update rapav
            $p_rapav->update();
        }
        $cn->commit();
    }

    /**
     * @brief display the name and description of the form
     *
     */
    function echo_formulaire()
    {
        $http=new \HttpInput();
        $ac=$http->request("ac");
        $plugin_code=$http->request("plugin_code");

        echo '<div id="form_description">';
        echo '<h1>'.h($this->f_title).'</h1>';
        echo '<p class="info">'.h($this->f_description).'<p>';
        echo '<p>';
        echo _('Modèle').' ';
        echo '<span class="span_rapav_template">';
        echo $this->anchor_document();
        echo '</span>';
        echo '</p>';
        echo \HtmlInput::button_action(_("Modifier"), sprintf("form_modify('%s')",$this->f_id));
        echo \HtmlInput::button_anchor(_('Export définition'),
                sprintf("extension.raw.php?plugin_code=%s&ac=%s&gDossier=%s&d_id=%s&act=rapav_form_export",$plugin_code,$ac,\dossier::id(),$this->f_id));

        echo '</div>';
        echo '<hr>';
    }

    /**
     * @brief input all the form detail of the current
     *
     */
    function input_parameter()
    {

        $http=new \HttpInput();
        $ac=$http->request("ac");
        $plugin_code=$http->request("plugin_code");
        $dossier_id=\Dossier::id();
        $max=count($this->definition);
        for ($i=0; $i<$max; $i++)
        {
            $obj=new Formulaire_Param($this->definition[$i]);
            printf('<div id="row_form_%s" posrow="%s">', $this->definition[$i]->p_id, $this->definition[$i]->p_order);
            $obj->input();
            $obj->add_button();
            echo '</div>';
        }
        if ($max==0)
        {
            $sql=new formulaire_param_sql(-1);
            $obj=new Formulaire_Param($sql);
            $obj->obj->f_id=$this->f_id;
            $obj->obj->p_order=10;
            $obj->add_button();
        }
    }

    /**
     * @brief remove a doc template
     */
    function remove_doc_template()
    {
        global $cn;
        $cn->lo_unlink($this->f_lob);
        $this->f_filename=null;
        $this->f_size=null;
        $this->f_mimetype=null;
        $this->f_lob=null;
        $this->update();
    }

    /**
     * @brief Create a random unique code for form params
     * @return string
     */
    static function generate_code()
    {
        $a_possible=explode(',', 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,0,1,2,3,4,5,6,7,8,9');
        $max=count($a_possible);
        $max--;
        $code=$a_possible[rand(0, $max)].
                $a_possible[rand(0, $max)].
                $a_possible[rand(0, $max)].
                $a_possible[rand(0, $max)].
                $a_possible[rand(0, $max)].
                $a_possible[rand(0, $max)].
                $a_possible[rand(0, $max)].
                $a_possible[rand(0, $max)].
                $a_possible[rand(0, $max)].
                $a_possible[rand(0, $max)].
                $a_possible[rand(0, $max)].
                $a_possible[rand(0, $max)]
        ;
        return$code;
    }

    /**
     * @brief export a form to a file
     * @global type $cn database connx
     * @param type $p_id the formulaire.f_id
     */
    static function export($p_id)
    {
        global $cn;
        $form=new formulaire_sql($p_id);
        $form->load();
        $title=mb_strtolower($form->f_title, 'UTF-8');
        $title=noalyss_str_replace(array('/', '*', '<', '>', '*', '.', '+', ':', '?', '!', " ", ";"), "_", $title);

        $out=fopen("php://output", "w");
        header('Pragma: public');
        header('Content-type: application/bin');
        header('Content-Disposition: attachment;filename="'.$title.'.bin"', FALSE);
        fputcsv($out, array("RAPAV", '4'), ";");
        fputcsv($out, array($form->f_title, $form->f_description), ";");
        $array=$cn->get_array("select p_id,p_code, p_libelle, p_type, p_order, f_id,  t_id
			from rapport_advanced.formulaire_param where f_id=$1", array($p_id));
        for ($i=0; $i<count($array); $i++)
        {
            fputcsv($out, $array[$i], ";");
        }
        fputcsv($out, array('RAPAV_DETAIL'), ";");
        $array=$cn->get_array("select
			fp_id, p_id, tmp_val, tva_id, fp_formula, fp_signed, jrn_def_type,
			tt_id, type_detail, with_tmp_val, type_sum_account, operation_pcm_val,date_paid
			from rapport_advanced.formulaire_param_detail where p_id in (select p_id from rapport_advanced.formulaire_param where f_id=$1)",
                array($p_id));
        for ($i=0; $i<count($array); $i++)
        {
            fputcsv($out, $array[$i], ";");
        }
    }

    /**
     * @brief Import a form from a file , usually in CSV
     * @global  $cn
     * @param string filename
     * @returns \Formulaire_SQL
     */
    static function import($filename)
    {
        global $cn;
        static $is_prepare = false;
        $in=fopen($filename, "r");
        $cn->start();
        try
        {
            $a=fgetcsv($in, 0, ";");
            if ($a[0]!="RAPAV")
            {
                throw new \Exception('Formulaire invalide');
            }
            // $a[1] contains the version
            $rapav_version=$a[1];
            // first line is the title and description
            $form=new formulaire_sql();
            $first=\fgetcsv($in, 0, ";");
            $form->f_title=$first[0];
            if (isset($first[1]))
                $form->f_description=$first[1];
            $form->f_description=$form->f_description." ".date("Ymd-H:i");
            $form->insert();
          
            // now come the formulaire_param until the keyword RAPAV_DETAIL is met
            while (($csv=\fgetcsv($in, 0, ";"))!=FALSE)
            {
                if ($csv[0]!="RAPAV_DETAIL")
                {
                    if ($rapav_version==2)
                    {
                        unset($csv[6]);
                    }
                    $csv[5]=$form->f_id;
                    $cn->get_array("INSERT INTO rapport_advanced.restore_formulaire_param(
						    p_id, p_code, p_libelle, p_type, p_order, f_id, t_id)
								VALUES ($1, $2, $3, $4, $5, $6, $7)", $csv);
                }
                else
                    break;
            }
            while (($csv=fgetcsv($in, 0, ";"))!=FALSE)
            {
                $t=array();
                for ($o=0; $o<count($csv); $o++)
                {
                    if ($csv[$o]=="")
                        $t[$o]=null;
                    else
                    {
                        $t[$o]=$csv[$o];
                    }
                }
                if ($rapav_version<4)
                {
                    $t[12]=0;
                }
                $t[13]=$form->f_id;
                $cn->get_array("INSERT INTO rapport_advanced.restore_formulaire_param_detail(
        fp_id, p_id, tmp_val, tva_id, fp_formula, fp_signed, jrn_def_type,
        tt_id, type_detail, with_tmp_val, type_sum_account, operation_pcm_val,date_paid,f_id)
                            VALUES ($1, $2, $3, $4, $5, $6, $7,$8, $9, $10, $11, $12,$13,$14)", $t);
            }

            /// Update now the table  rapport_advanced.restore_formulaire_param and set the correct pk
             //$cn->exec_sql("update rapport_advanced.restore_formulaire_param set p_id=nextval('rapport_advanced.formulaire_param_p_id_seq')");
             $cn->exec_sql("update rapport_advanced.restore_formulaire_param set p_code=null where p_code=''");
            // Insert row by row + detail
            $array=$cn->get_array("select p_id,p_code,p_libelle,p_order,f_id,t_id from rapport_advanced.restore_formulaire_param where f_id=$1",
                    array($form->f_id));
            // Prepare stmt for the details
            if ( $is_prepare == false)
            {
                $cn->prepare('detail',
                        'select p_id,tmp_val,tva_id,fp_formula,fp_signed, jrn_def_type,tt_id,type_detail,with_tmp_val,type_sum_account,operation_pcm_val,date_paid
                                        from  rapport_advanced.restore_formulaire_param_detail where p_id=$1');
                $is_prepare=true;

            }
            $nb=count($array);
            
            for ($e=0; $e<$nb; $e++)
            {
                // Insert first into  rapport_advanced.formulaire_param
                $new_pid=$cn->get_value("insert into rapport_advanced.formulaire_param (p_code, p_libelle, p_type, p_order, f_id, t_id)
                                   select   p_code, p_libelle, p_type, p_order, f_id, t_id
				from rapport_advanced.restore_formulaire_param where p_id=$1 returning p_id", array($array[$e]['p_id']));
                // Insert detail
                $cn->exec_sql("insert into rapport_advanced.formulaire_param_detail
                                        (fp_id,
                                        p_id,
                                        tmp_val,
                                        tva_id,
                                        fp_formula,
                                        fp_signed,
                                        jrn_def_type,
                                        tt_id,
                                        type_detail,
                                        with_tmp_val,
                                        type_sum_account,
                                        operation_pcm_val,
                                        jrn_def_id,
                                        date_paid
                                        )
                                        select
                                        nextval('rapport_advanced.formulaire_param_detail_fp_id_seq'),
                                        $new_pid,
                                            tmp_val,
                                            tva_id,
                                            fp_formula,
                                            fp_signed,
                                            jrn_def_type,
                                            tt_id,
                                            type_detail,
                                            with_tmp_val,
                                            type_sum_account,
                                            operation_pcm_val,
                                            -1,
                                            date_paid
                                            from
                                            rapport_advanced.restore_formulaire_param_detail where p_id =$1
                                ", array($array[$e]['p_id']));
            }


            $cn->exec_sql('delete from  rapport_advanced.restore_formulaire_param where f_id=$1', array($form->f_id));
            $cn->exec_sql('delete from  rapport_advanced.restore_formulaire_param_detail where f_id=$1', array($form->f_id));
            $cn->commit();
            return $form;
        }
        catch (\Exception $exc)
        {
            echo $exc->getMessage();
            \record_log($exc->getMessage());
            \record_log($exc->getTraceAsString());
            throw $exc;
        }
    }
    /**
     * There is an issue when converting in PDF thank the unoconv utility, it fails with space or accentuated charactere
     * 
     * @param type $p_filename
     */
    static  function normalize_filename($p_filename)
    {
        $result=noalyss_str_replace(["à","â","ä"],"a",$p_filename);
        $result=noalyss_str_replace(["é","è","ë","ê"],"e",$result);
        $result=noalyss_str_replace(["ù","ü","û"],"u",$result);
        $result=noalyss_str_replace(["ô","ö"],"o",$result);
        $result=noalyss_str_replace("ç","c",$result);
        $result=noalyss_str_replace(" ","_",$result);
        return $result;
    }

}
