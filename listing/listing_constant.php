<?php 
//This file is part of NOALYSS and is under GPL 
//see licence.txt
/**
 *@file
 *Contains all the needed variable for the plugin
 *is name is plugin_name_constant.php
 * You can use some globale variable, especially for the database
 *  connection
 */

require_once NOALYSS_INCLUDE.'/class/database.class.php';

global $cn,$listing_version,$errcode;
global $g_listing_home ;
global $g_dossier,$g_access,$g_plugin_code;
$http=new \HttpInput();
$g_dossier=Dossier::id();
$g_access=$http->request("ac");
$g_plugin_code=$http->request("plugin_code");

$g_listing_home=__DIR__;
$cn=Dossier::connect();

// to upgrade the SQL schema
$listing_version=1;
?>
