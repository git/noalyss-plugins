<?php
//This file is part of NOALYSS and is under GPL 
//see licence.txt
$http=new HttpInput();
$lc_id=$http->get("lc_id","number");
?>
<div class="content">
    <?php \Noalyss\Dbg::echo_file(__FILE__)?>
<form onsubmit="send_email(); return false;" id="parameter_send_email_input_frm"  method="post" enctype="multipart/form-data">
<?php
    echo HtmlInput::array_to_hidden(array('gDossier','plugin_code','ac','lc_id'), $_REQUEST);
    echo HtmlInput::hidden('act','send_mail');
?>

<p>
    <label for="p_from">De </label><?php echo $from->input();?>
</p>
<p>
    <label for="p_subject">Sujet</label><?php echo $subject->input();?>
</p>
<p>
    <label for="p_message">Message</label><?php echo $message->input();?>
</p>

<p>
    <label for="p_attach">Attache</label><?php echo $attach->input();?>
</p>

<p >
    <label for="copy">Copie pour l'expéditeur</label><?php echo $copy->input();?>
</p>
<?php

// upload files
RAPAV_Listing_Compute::file_upload($file_id,$lc_id);


echo HtmlInput::submit("send_mail", _('Envoi'), '', 'smallbutton');
echo HtmlInput::button_close('parameter_send_mail_input');
?>
</form>
</div>