<?php
//This file is part of NOALYSS and is under GPL 
//see licence.txt
$http=new \HttpInput();
?>
<table class="result" id="listing_tb_id">
    <tr>
        <th>
            
        </th>
        <th>
            Nom
        </th>
        <th>
            Description
        </th>
        <th>
            Modèle de document
        </th>
        <th>
            Catégorie de fiches
        </th>
    </tr>
    <?php
    $max=Database::num_row($res);
    $ac=$http->request('ac');
    $plugin_code= $http->request('plugin_code');
    $dossier=Dossier::id();
    
    for ($i=0;$i<$max;$i++):
            $class=($i%2==0)?'class="odd"':'class="even"';

        $row=  Database::fetch_array($res, $i);
        $arg = array(
            'gDossier' => $dossier,
            'ac' =>$ac,
            'pc' =>$plugin_code,
            'id' => $row['l_id'],
            'cin' => 'listing_definition_id',
            'cout' => 'listing_definition_div_id');
        $json = 'listing_definition(' . noalyss_str_replace('"', "'", json_encode($arg)) . ')';
        
        $arg2=array(
            'gDossier' => $dossier,
            'ac' =>$ac,
            'pc' =>$plugin_code,
            'id' => $row['l_id'],
            'cin' => 'listing_detail_id',
            'cout' => 'listing_detail_id');
        $modify = 'listing_modify('.noalyss_str_replace('"',"'",  json_encode($arg2).')');
        $url_document="extension.raw.php?".http_build_query(array(
            'gDossier' => $dossier,
            'ac' =>$ac,
            'plugin_code' =>$plugin_code,
            'act'=>'downloadTemplateListing',
            "id"=>$row['l_id']
           ));
    ?>
    	<tr <?php echo $class; ?>>
            <td>
            <?php echo Icon_Action::modify(uniqid(), $modify); ?>
            </td>
        <td>
            <?php echo HtmlInput::anchor(h($row['l_name']),'',' onclick="'.$json.'"'); ?>
        </td>
        <td>
            <?php echo h($row['l_description']); ?>
        </td>
        <td>
           <?php echo HtmlInput::anchor(h($row['l_filename']), $url_document);?>
        </td>
        <td>
            <?php echo h($row['fd_label']); ?>
        </td>
        <td>
            <?php
            $url=http_build_query(["ac"=>$ac,
                    "plugin_code"=>$plugin_code,
                    "gDossier"=>$dossier,
                    "sa"=>"de",
                    "listing_id"=>$row['l_id'] ]);
           $button=sprintf('<A style="font-family:fontello;font-weight:normal;cursor: pointer;text-decoration:none;"'
                   . ' href="?%s"  >&#xe82e;</A>',
                $url);
           echo $button;
            ?>
        </td>
    </tr>
    <?php
    endfor;
    
    ?>
</table>