<?php
global $g_listing_home;
require_once $g_listing_home."/db/listing_document_sql.php";
require_once $g_listing_home.'/include/rapav_listing_compute.class.php';
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Copyright Author Dany De Bontridder danydb@aevalys.eu 20/07/23
/*!
 * \file
 * \brief upload files or allow to delete some
 */

global $g_dossier,$g_access,$g_plugin_code,$cn,$g_listing_home;
$http=new HttpInput();
$act2=$http->request("act2","string","");

// if we ask to delete a file
if ( $act2 == 'df') {
    $listing_document=new Listing_Document_SQL($cn,$http->request("file_delete"));
    $listing_document->delete();


}

$bucket_id=$http->request("file_id");
$listing_id=$http->request("listing_id");
html_min_page_start($_SESSION[SESSION_KEY.'g_theme']);
?>

<div CLASS="op_detail_frame" >

<span>
    <?=_("Choisissez un ou plusieurs fichiers")?>
</span>
<?php
global $cn;
// if files , then upload them
if ( ! empty ($_FILES)) {
    $cnt=count($_FILES["listing_document"]['name']);
    $cn->start();

    for ($i = 0; $i < $cnt;$i++)
    {
        $new_name=tempnam($_ENV['TMP'],'listing_');
        // check if a file is submitted
        if ( strlen($_FILES['listing_document']['tmp_name'][$i]) != 0 )
        {
            // upload the file and move it to temp directory
            if ( move_uploaded_file($_FILES['listing_document']['tmp_name'][$i],$new_name))
            {
                $oid=$cn->lo_import($new_name);
                // check if the lob is in the database
                if ( $oid == false )
                {
                    printf( _("%s erreur chargement"),  $_FILES['listing_document']['name']);
                }else {
                    // the upload in the database is successfull
                    $listing_document = new Listing_Document_SQL($cn);
                    $listing_document->setp("ld_filename",$_FILES['listing_document']['name'][$i]);
                    $listing_document->setp("ld_mimetype",$_FILES['listing_document']['type'][$i]);
                    $listing_document->setp("ld_size",$_FILES['listing_document']['size'][$i]);
                    $listing_document->setp("bucket_id",$bucket_id);
                    $listing_document->setp("listing_id",$listing_id);
                    $listing_document->setp("ld_lob",$oid);
                    $listing_document->insert();
                }
            }
        }
    } /* end for */
    $cn->commit();
}


// Show the files currently loaded for this email
$a_file = $cn->get_array("select * from rapport_advanced.listing_document where listing_id=$1", [$listing_id]);
if ( ! empty($a_file)) {
    $sum=0;
    echo '<ol>';
    foreach ($a_file as $file) {
        printf ( '<li id="file%s">',$file['ld_id']);
        $arg=array("gDossier"=>Dossier::id(),
            "plugin_code"=>$http->request('plugin_code'),
            "ac"=>$http->request('ac'),
            'act'=>'show_attached',
            'ld_id'=>$file['ld_id']);

        $href=  "extension.raw.php?".http_build_query($arg);

        echo '<a href="'.$href.'" >';
        echo strip_tags($file['ld_filename']);
        echo '</a>';

        echo \Icon_Action::trash(uniqid(),    sprintf('listing_delete_file(%s)',$file['ld_id']) );
        echo '</li>';
        $sum+=$file['ld_size'];
        \Noalyss\Dbg::echo_var(1, "file {$file['ld_size']})");
    }
    echo '</ol>';
    $sum_mb = round($sum/10000)/100;
    printf(_("Total MB %s"),$sum_mb);
    if ( $sum > \RAPAV_Listing_Compute::$max_email_size ) {
        echo_warning(_("Limite email = 2MB supprimez ou compressez les fichiers"));
    }

}
?>
<FORM method="POST" ACTION="ajax.php?act=upload_file" enctype="multipart/form-data">
    <?php
    echo \HtmlInput::array_to_hidden(array("ac", "plugin_code", "gDossier", "file_id","act","listing_id"), $_REQUEST);
    echo \HtmlInput::hidden("MAX_FILE_SIZE", MAX_FILE_SIZE);
    ?>
    <input type="file" name="listing_document[]" onchange="submit(this)" multiple>

</FORM>
</div>

<script>
    var dossier=<?=$g_dossier?>;
    var plugin_code='<?=$g_plugin_code?>';
    var ac='<?=$g_access?>';
    /**
     * @brief drop file from listing_document
     * @param p_fileid
     */
    function listing_delete_file(p_fileid) {
        try
        {

            var queryString={'act':'upload_file','act2':'df','file_delete':p_fileid,
                'plugin_code':plugin_code,'ac':ac,'gDossier':dossier,'file_id':'<?=$bucket_id?>',
                'listing_id':'<?=$listing_id?>'
            };
            var action = new Ajax.Request(
                "ajax.php" ,
                {
                    method:'get',
                    parameters:queryString,
                    onFailure:ajax_misc_failure,
                    onSuccess:function(req){
                        $('file_upload_div').innerHTML=req.responseText

                    }
                }
            );
        }catch( e)
        {
           console.error(e.message);
        }
    }

</script>