<?php
//This file is part of NOALYSS and is under GPL 
//see licence.txt

require_once $g_listing_home.'/include/rapav_listing_compute.class.php';

ob_start();
$listing=new RAPAV_Listing_Compute();
$listing->load($lc_id);
$tcopy=(isset($copy))?1:0;
$error="";
echo HtmlInput::title_box(_('Résultat'),'parameter_send_mail_result');


try {
    $file_to_send=$listing->export_attached_file();
    $a_result=$listing->send_mail($p_from,$p_subject,$p_message,$p_attach,$tcopy,$file_to_send);


    echo '<ol>';
    for ($i=0;$i<count($a_result);$i++)
    {
        echo '<li>'.$a_result[$i].'</li>';
    }
    echo '</ol>';
} catch (\Exception $e) {
    printf(_("Email non envoyé "));
    echo_warning($e->getMessage());
}
echo HtmlInput::button_close('parameter_send_mail_result');
$response = ob_get_clean();
$html = escape_xml($response);
header('Content-type: text/xml; charset=UTF-8');
echo <<<EOF
<?xml version="1.0" encoding="UTF-8"?>
<data>
<ctl></ctl>
<code>$html</code>
</data>
EOF;
?>        