<?php
//This file is part of NOALYSS and is under GPL 
//see licence.txt

/**
 * Display form to enter parameters
 */
require_once $g_listing_home.'/include/rapav_listing_compute.class.php';
require_once NOALYSS_INCLUDE.'/class/fiche_def.class.php';
$http=new HttpInput();
ob_start();
$compute=new RAPAV_Listing_Compute();
$compute->load($http->request('lc_id',"number"));

$fiche_def=new Fiche_Def($cn,$compute->listing->data->getp('fiche_def_id'));
if ( $fiche_def->HasAttribute(ATTR_DEF_EMAIL) == false) {
    echo '<p class="notice">';
    echo _("Cette catégorie n'a pas d'attribut email");
    echo '</p>';
} else {
    echo HtmlInput::title_box(_('Envoi par email'), "parameter_send_mail_input");
    $subject=new IText('p_subject');
    $from=new IText('p_from');
    $message=new ITextarea('p_message');

    $copy=new ICheckBox('copy');
    //-----
    // Propose to generate document to attach at the email ,
    // if there is a template
    if ( $compute->has_template() )
    {
        $attach=new ISelect('p_attach');
        $attach->value=array (
                array('value'=>0,'label'=>_('Aucun document')),
                array('value'=>1,'label'=>_('Document en PDF')),
                array('value'=>2,'label'=>_('Document généré'))
        );
        
    } else {
        $attach=new IHidden("p_attach",0);
    }
    $file_id=uniqid();
    echo htmlInput::hidden("file_id", $file_id);
    require_once $g_listing_home.'/template/parameter_send_mail_input.php';


}
$response = ob_get_clean();
$html = escape_xml($response);
header('Content-type: text/xml; charset=UTF-8');
echo <<<EOF
<?xml version="1.0" encoding="UTF-8"?>
<data>
<ctl></ctl>
<code>$html</code>
</data>
EOF;
?>        