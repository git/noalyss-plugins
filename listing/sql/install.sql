begin;

CREATE TABLE rapport_advanced.version_listing (
                           version_id int4 NOT NULL,
                           version_date timestamp NULL DEFAULT now(),
                           version_note text NULL,
                           CONSTRAINT version_listing_pkey PRIMARY KEY (version_id)
);

insert into rapport_advanced.version_listing values (0,now(),'install listing SQL');
commit;
