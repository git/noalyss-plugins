<?php
//This file is part of NOALYSS and is under GPL 
//see licence.txt
$http=new \HttpInput();
?>
<div class="content" id="listing_definition_div_id">
    <?php
    /**
     * @file
     * @brief manage the listing
     */
    require_once 'rapav_listing.class.php';
    global $cn;
    $listing = new Rapav_Listing();
    /**
     * if cloning request
     */
    $listing_id=$http->request("l_id","number",0);
    if ( isset ($_POST['listing_clone']) )
    {
        if ($listing_id == 0 )
            throw new Exception('Invalide');
        
        $old=new Rapav_Listing($listing_id);
        $new = $old->make_clone();
        $new->display();
        echo '<p>';
        $new->button_add_param();
        echo '</p>';
        return;
    }
    /**
     * save new listing
     */
    if (isset($_POST['listing_add_sb']))
    {
        $new = new Rapav_Listing($listing_id);
        if (!isset($_POST['remove']))
        {
            $new->save($_POST);
            $new->display();
            echo '<p>';
            $new->button_add_param();
            echo '</p>';
            
            return;
        } else
            $new->delete($_POST);
    }

///////////////////////////////////////////////////////////////////////////////
//Listing
///////////////////////////////////////////////////////////////////////////////
    $listing->to_list();
    echo '<p>';
    Rapav_Listing::Button_Add_Listing();
    echo '</p>';
    ?>
</div>