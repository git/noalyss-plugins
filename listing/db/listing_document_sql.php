<?php

/**
 *   This file is part of NOALYSS
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * copyright Dany WM De Bontridder($(YEAR))
 *
 *  $(DATE)
 */

/**
 * @file
 * @brief abstract of the table
 */

/**
 * @class
 * @brief  abstract of the table
 */
class Listing_Document_SQL extends Table_Data_SQL
{

    function __construct(DatabaseCore $p_cn, $p_id = -1)
    {
        $this->table = "rapport_advanced.listing_document";
        $this->primary_key = "ld_id";
        /*
         * List of columns
         */

        $this->name = array(
            "ld_id" => "ld_id",
            "bucket_id" => "bucket_id",
            "ld_filename"=>"ld_filename",
            "ld_mimetype"=>"ld_mimetype",
            "ld_size"=>"ld_size",
            "listing_id"=>"listing_id",
            "ld_lob"=>"ld_lob"
        );
        /*
         * Type of columns
         */
        $this->type = array(
            "ld_id" => "numeric",
            "bucket_id" => "text",
            "ld_filename"=>"text",
            "ld_mimetype"=>"text",
            "ld_size"=>"numeric",
            "listing_id"=>"numeric",
            "ld_lob"=>"text"

        );


        $this->default = array(
            "ld_id" => "auto"
        );

        $this->date_format = "DD.MM.YYYY";
        parent::__construct($p_cn, $p_id);
    }


}