<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/* !\file
 * \brief save the operation
 */
require_once NOALYSS_INCLUDE.'/class/acc_ledger_purchase.class.php';
require_once NOALYSS_INCLUDE.'/class/acc_ledger_sale.class.php';
require_once('modop_operation.class.php');
$http=new \HttpInput();
$_POST['jr_optype']='NOR';
/* ---------------------------------------------------------------------- */
// Purchase
/* ---------------------------------------------------------------------- */
if ($_POST['jrn_type']=='ACH')
{
    $jrn=new Acc_Ledger_Purchase($cn, $http->post('p_jrn','number'));
    try
    {
        $op=new Modop_Operation($cn, $http->post("ext_jr_internal"));
        $op->suspend_receipt();
        $pj=$_POST['e_pj'];
        $oldpj=microtime();
        $cn->exec_sql('update jrn set jr_pj_number=$1  where jr_id=$2', array($oldpj,  $http->post("ext_jr_id")));
        // get the old repo to save the stock
        $repo=$cn->get_value("select distinct r_id from stock_goods 
                where 
                j_id in (select j_id from jrnx join jrn on j_grpt=jr_grpt_id where jr_id=$1)",
                    array($http->post("ext_jr_id")));
        if ($repo != "") {
            $_POST['repo']=$repo;
        }
        $new_internal=$jrn->insert($_POST);
    }
    catch (Exception $e)
    {
        alert($e->getMessage());
        exit();
    }
    $cn->commit();

    /* we delete the old operation */
    $cn->start();
    /* in stock_goods */
    $cn->exec_sql('delete from stock_goods where j_id in (select j_id from jrnx join jrn on (j_grpt=jr_grpt_id) where jr_id=$1)',
            array( $http->post("ext_jr_id")));

    /* in jrnx */
    $cn->exec_sql('delete from jrnx where j_grpt in (select jr_grpt_id from jrn where jr_id=$1)',
            array( $http->post("ext_jr_id")));

    /* in jrn, retrieve info to reattach the receipt */
    $attach=$cn->get_array('select jr_pj,jr_pj_name,jr_pj_type from jrn where jr_id=$1', array( $http->post("ext_jr_id")));

    // delete old data from jrn
    $cn->exec_sql('delete from jrn where jr_id=$1', array($http->post("ext_jr_id")));

    $cn->exec_sql('update jrn set jr_id=$1,jr_internal=$2,jr_pj_number=$3 where jr_internal=$4',
            array( $http->post("ext_jr_id"),  $http->post("ext_jr_internal"), $pj, $new_internal));
    if ($attach[0]['jr_pj_name']!=''&&!isset($_POST['gen_invoice']))
    {
        $cn->exec_sql('update jrn set jr_pj=$1,jr_pj_type=$2,jr_pj_name=$3 where jr_id=$4',
                array($attach[0]['jr_pj'], $attach[0]['jr_pj_type'], $attach[0]['jr_pj_name'],  $http->post("ext_jr_id")));
    }
    /* in quant_purchase */
    $cn->exec_sql('update quant_purchase set qp_internal=$1 where qp_internal=$2',
            array( $http->post("ext_jr_internal"), $new_internal));

    $cn->commit();
    echo '<h2 class="info"> '._('Enregistrement').' </h2>';
    echo "<h2 >"._('Opération sauvée'). $http->post("ext_jr_internal");
    if ($jrn->pj!='')
        echo ' Piece : '.h($jrn->pj);
    echo "</h2>";
    if (strcmp($jrn->pj, $_POST['e_pj'])!=0)
    {
        echo '<h3 class="notice"> '._('Attention numéro pièce existante, elle a du être adaptée').'</h3>';
    }
    /* Save the additional information into jrn_info */
    $obj=new Acc_Ledger_Info($cn);
    $obj->save_extra($jrn->jr_id, $_POST);
    printf('<a class="line" style="display:inline" href="javascript:modifyOperation(%d,%d)">%s</a><hr>',
             $http->post("ext_jr_id"), dossier::id(), $new_internal);
    // Feedback
    $jrn->internal= $http->post("ext_jr_internal");
    echo $jrn->confirm($_POST, true);
    if (isset($jrn->doc))
    {
        echo '<span class="invoice">';
        echo $jrn->doc;
        echo '</span>';
    }

    echo '</div>';
}
/* ---------------------------------------------------------------------- */
// SOLD
/* ---------------------------------------------------------------------- */
if ($_POST['jrn_type']=='VEN')
{
    $jrn=new Acc_Ledger_Sale($cn, $http->post('p_jrn','number'));
    $pj=$_POST['e_pj'];
    try
    {
        $cn->start();
        $op=new Modop_Operation($cn,  $http->post("ext_jr_internal"));
        $op->suspend_receipt();

        $oldpj=microtime();
        $cn->exec_sql('update jrn set jr_pj_number=$1  where jr_id=$2', array($oldpj,  $http->post("ext_jr_id")));
        // get the old repo to save the stock
        $repo=$cn->get_value("select distinct r_id from stock_goods 
                where 
                j_id in (select j_id from jrnx join jrn on j_grpt=jr_grpt_id where jr_id=$1)",
                    array($http->post("ext_jr_id")));
        if ($repo != "") {
            $_POST['repo']=$repo;
        }
        $new_internal=$jrn->insert($_POST);
    }
    catch (Exception $e)
    {
        alert($e->getMessage());
        exit();
    }
    $cn->commit();
    /* we delete the old operation */
    $cn->start();

    /* in stock_goods */
    $cn->exec_sql('delete from stock_goods where j_id in (select j_id from jrnx join jrn on (j_grpt=jr_grpt_id) where jr_id=$1)',
            array( $http->post("ext_jr_id")));

    /* in jrnx */
    $cn->exec_sql('delete from jrnx where j_grpt in (select jr_grpt_id from jrn where jr_id=$1)',
            array( $http->post("ext_jr_id")));

    /* in jrn */
    $attach=$cn->get_array('select jr_pj,jr_pj_name,jr_pj_type from jrn where jr_id=$1', array( $http->post("ext_jr_id")));

    $cn->exec_sql('delete from jrn where jr_id=$1', array( $http->post("ext_jr_id")));
    $cn->exec_sql('update jrn set jr_id=$1,jr_internal=$2,jr_pj_number=$3 where jr_internal=$4',
            array( $http->post("ext_jr_id"),  $http->post("ext_jr_internal"), $pj, $new_internal));

    if ($attach[0]['jr_pj_name']!=''&&!isset($_POST['gen_invoice']))
    {
        $cn->exec_sql('update jrn set jr_pj=$1,jr_pj_type=$2,jr_pj_name=$3 where jr_id=$4',
                array($attach[0]['jr_pj'], $attach[0]['jr_pj_type'], $attach[0]['jr_pj_name'],  $http->post("ext_jr_id")));
    }

    /* in quant_sold */
    $cn->exec_sql('update quant_sold set qs_internal=$1 where qs_internal=$2',
            array( $http->post("ext_jr_internal"), $new_internal));

    $cn->commit();
    /* Show button  */
    echo '<h2 class="info"> Enregistrement </h2>';
    $jr_id= $http->post("ext_jr_id");

    echo "<h2 >"._('Opération sauvée');
    if ($jrn->pj!='')
        echo ' Piece : '.h($jrn->pj);
    echo "</h2>";
    if (strcmp($jrn->pj, $_POST['e_pj'])!=0)
    {
        echo '<h3 class="notice"> '._('Attention numéro pièce existante, elle a du être adaptée').'</h3>';
    }

    printf('<a class="line" style="display:inline" href="javascript:modifyOperation(%d,%d)">%s</a><hr>', $jr_id,
            dossier::id(), $new_internal);
    $jrn->internal= $http->post("ext_jr_internal");
    echo $jrn->confirm($_POST, true);
    /* Show link for Invoice */
    if (isset($jrn->doc))
    {
        echo '<span class="invoice">';
        echo $jrn->doc;
        echo '</span>';
    }


    /* Save the additional information into jrn_info */
    $obj=new Acc_Ledger_Info($cn);
    $obj->save_extra($jr_id, $_POST);

    echo '</div>';
}
/* ---------------------------------------------------------------------- */
// ODS
/* ---------------------------------------------------------------------- */
if ($_POST['jrn_type']=='ODS')
{
    $jrn=new Acc_Ledger($cn, $http->post('p_jrn','number'));
    try
    {
        $op=new Modop_Operation($cn,  $http->post("ext_jr_internal"));
        $op->suspend_receipt();
        $pj=$_POST['e_pj'];
        $_POST['e_pj']=microtime();
        $jrn->save($_POST);
        $new_internal=$jrn->internal;
    }
    catch (Exception $e)
    {
        alert($e->getMessage());
        exit();
    }
    /* we delete the old operation */
    $cn->start();

    /* in jrnx */
    $cn->exec_sql('delete from jrnx where j_grpt in (select jr_grpt_id from jrn where jr_id=$1)',
            array( $http->post("ext_jr_id")));

    /* in jrn */
    $attach=$cn->get_array('select jr_pj,jr_pj_name,jr_pj_type from jrn where jr_id=$1', array( $http->post("ext_jr_id")));
    $cn->exec_sql('delete from jrn where jr_id=$1', array( $http->post("ext_jr_id")));
    $cn->exec_sql('update jrn set jr_id=$1,jr_internal=$2,jr_pj_number=$3 where jr_internal=$4',
            array( $http->post("ext_jr_id"),  $http->post("ext_jr_internal"), $pj, $new_internal));
    if ($attach[0]['jr_pj_name']!='')
    {
        $cn->exec_sql('update jrn set jr_pj=$1,jr_pj_type=$2,jr_pj_name=$3 where jr_id=$4',
                array($attach[0]['jr_pj'], $attach[0]['jr_pj_type'], $attach[0]['jr_pj_name'],  $http->post("ext_jr_id")));
    }

    $cn->commit();
}

/* ---------------------------------------------------------------------- */
// Purchase
/* ---------------------------------------------------------------------- */
if ($_POST['jrn_type']=='FIN')
{
    extract($_POST, EXTR_SKIP);
    $user=new Noalyss_user($cn);
    try
    {
        /*  verify if the card can be used in this ledger */
        if ($user->check_jrn($p_jrn)!='W')
            throw new Exception(_('Accès interdit'), 20);
        /* check if there is a customer */
        if (noalyss_strlentrim($e_bank_account)==0)
            throw new Exception(_('Vous n\'avez pas donné de banque'), 11);

        /*  check if the date is valid */
        if (isDate($e_date)==null)
        {
            throw new Exception('Date invalide', 2);
        }
        $ledger=new Acc_Ledger_Fin($cn,$p_jrn);
        if ( $ledger->get_currency()->get_id() > 0 ){
            throw new Exception ( _("Désolé impossible de modifier les journaux financier en devise") );
        }
        $fiche=new fiche($cn);
        $fiche->get_by_qcode($e_bank_account);
        if ($fiche->empty_attribute(ATTR_DEF_ACCOUNT)==true)
            throw new Exception('La fiche '.$e_bank_account.'n\'a pas de poste comptable', 8);
        if ($fiche->belong_ledger($p_jrn, 'cred')!=1&&$fiche->belong_ledger($p_jrn, 'deb')!=1)
            throw new Exception('La fiche '.$e_bank_account.'n\'est pas accessible à ce journal', 10);
        $fiche=new fiche($cn);
        $fiche->get_by_qcode($e_other);
        if ($fiche->empty_attribute(ATTR_DEF_ACCOUNT)==true)
            throw new Exception('La fiche '.$e_other.'n\'a pas de poste comptable', 8);
        if ($fiche->belong_ledger($p_jrn, 'deb')!=1)
            throw new Exception('La fiche '.$e_other.'n\'est pas accessible à ce journal', 10);
        if (isNumber($ {'e_other_amount'})==0)
            throw new Exception('La fiche '.$e_other.'a un montant invalide ['.$e_other_amount.']', 6);
    }
    catch (Exception $e)
    {
        echo h2($e->getMessage() ,'class="error"');
        exit();
    }

    try
    {
        $cn->start();
        /* find periode thanks the date */
        $periode=new Periode($cn);
        $periode->find_periode($e_date);
        if ($periode->is_closed())
            throw new Exception('Période fermée');

        /* update amount */
        $cn->exec_sql("update jrnx set j_montant=$1,j_jrn_def=$3,j_date=to_date($4,'DD.MM.YYYY'),j_tech_per=$5,j_tech_date=now() where j_grpt in (select jr_grpt_id from jrn where jr_id=$2)",
                array(abs($e_other_amount), $ext_jr_id, $p_jrn, $e_date, $periode->p_id));

        /* in jrn */
        $cn->exec_sql("update jrn set jr_montant=$1,jr_comment=$2,jr_date=to_date($3,'DD.MM.YYYY'),jr_def_id=$4,jr_tech_per=$5,jr_pj_number=$6,jr_tech_date=now() where jr_id=$7",
                array(abs($e_other_amount), $e_other_comment, $e_date, $p_jrn, $periode->p_id, $e_pj, $ext_jr_id));
        /* in quant_fin */
        /* find the f_id of the bank */
        $fbank=new Fiche($cn);
        $fbank->get_by_qcode($e_bank_account);
        $post_bank=$fbank->strAttribut(ATTR_DEF_ACCOUNT);

        $fother=new Fiche($cn);
        $fother->get_by_qcode($e_other);
        $post_other=$fother->strAttribut(ATTR_DEF_ACCOUNT);
        if ($e_other_amount>0)
        {
            $cn->exec_sql('update jrnx set j_poste=$1,j_qcode=$2 where j_debit=false and j_grpt in (select jr_grpt_id from jrn where jr_id=$3)',
                    array($post_other, $e_other, $ext_jr_id));
            $cn->exec_sql('update jrnx set j_poste=$1,j_qcode=$2 where j_debit=true  and j_grpt in (select jr_grpt_id from jrn where jr_id=$3)',
                    array($post_bank, $e_bank_account, $ext_jr_id));
        }
        else
        {
            $cn->exec_sql('update jrnx set j_poste=$1,j_qcode=$2 where j_debit=false  and j_grpt in (select jr_grpt_id from jrn where jr_id=$3)',
                    array($post_bank, $e_bank_account, $ext_jr_id));
            $cn->exec_sql('update jrnx set j_poste=$1,j_qcode=$2 where j_debit=true  and j_grpt in (select jr_grpt_id from jrn where jr_id=$3)',
                    array($post_other, $e_other, $ext_jr_id));
        }
        $cn->exec_sql('update quant_fin set qf_bank=$1,qf_amount=$3,qf_other=$2 where jr_id=$4',
                array($fbank->id, $fother->id, $e_other_amount, $ext_jr_id));
        $cn->commit();
    }
    catch (Exception $e)
    {
        $cn->rollback();
        echo $e->getMessage();
        exit();
    }
    echo h2info('Opération sauvée');
}
