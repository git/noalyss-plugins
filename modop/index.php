<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/*!\file
 * \brief modify a operation
 */
require_once NOALYSS_INCLUDE.'/class/pre_operation.class.php';
require_once NOALYSS_INCLUDE.'/class/pre_op_ods.class.php';
require_once NOALYSS_INCLUDE.'/class/pre_op_ach.class.php';
require_once NOALYSS_INCLUDE.'/class/pre_op_fin.class.php';
require_once NOALYSS_INCLUDE.'/class/pre_op_ven.class.php';
global $version_plugin;
$http=new HttpInput();
$version_plugin=SVNINFO;
Extension::check_version(7000);
$str=new IConcerned("jr_id");
if (method_exists($str , "set_singleOperation")) {
    $str->set_singleOperation(TRUE);
}
$str->value=(isset($_GET['jr_id']))?strtoupper($http->get('jr_id') ):'';
$str->value=(isset($_GET['ext_jr_id']))?strtoupper($http->get('ext_jr_id' ) ):$str->value;
echo '<div style="float:right"><a class="mtitle" style="font-size:140%" href="http://wiki.noalyss.eu/doku.php?id=modification_d_operation" target="_blank">Aide</a>'.
'<span style="font-size:0.8em;color:red;display:inline">vers:SVNINFO</span>'.
'</div>';

global $g_parameter;
if ( $g_parameter->MY_STRICT=='Y') 
{
    echo h2(_("Désolé vous ne pouvez pas utiliser ce plugin , vous êtes en mode STRICT"),'class="error"');
    return;
}
if ( $str->value !="" && strpos(",", $str->value) !== 0 ){
    $tmp_value=explode(",",$str->value);
    $str->value=$tmp_value[0];
    
}
if ( trim($str->value ) != "" && isNumber($str->value) == 0 ) {
    echo h2(_("Opération invalide"),'class="error"');
    $str->value=0;
}
?>
<FORM METHOD="GET">
  <?php echo HtmlInput::extension().dossier::hidden();?>
  <?php echo HtmlInput::hidden('ac',$http->request('ac') );?>
<?php echo _("Code interne de l'opération à modifier") ?>
  <?php echo $str->input()?>
	<?php 

	echo HtmlInput::submit('seek','retrouver')?>
</FORM>
<hr>
<?php
$action=(isset ($_REQUEST['action']))?$_REQUEST['action']:'end';

  if ( ! isset ($_REQUEST['action']) && isset($_GET['seek'])) {
    /* retrieve and show the accounting */
    if ( $str->value=='') {
      alert(_('Aucune opération demandée')); exit;}
    /*  retrieve and display operation */
      
    require_once('modop_display.php');
    ?>
<script>
$('jr_id').value="<?php echo trim($http->get("jr_id","string",""));?> ";
</script>
<?php
    exit();
  }


/* we need to confirm it */
if ( $action=='confirm' ) {
  require_once('modop_confirm.php');
  exit();
}
/*  we can now save it */
if ($action=='save') {
  require_once ('modop_save.php');
  exit();
}
?>

