<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be
require_once 'acc_ledger_sale_generate.class.php';
require_once NOALYSS_INCLUDE.'/class/acc_ledger_search.class.php';
$cn=Dossier::connect();


$ledger = new Acc_Ledger_Sale_Generate($cn, -1);
$http=new HttpInput();
global $g_access,$g_dossier,$g_plugin_code;

$g_access=$http->request("ac");
$g_dossier=$http->request("gDossier","number");
$g_plugin_code=$http->request("plugin_code");

$_GET['ledger_type'] = 'VEN';
//var_dump($_GET);
$request = HtmlInput::default_value_request('action', -2);
$request = $http->request('action',"number", -2);
if ($request <> -2)
{
    if (!isset($_REQUEST['sel_sale']))
    {
        echo_warning(_("Aucune sélection faite"));
    } else
    {
        switch ($request)
        {
            case 1:
                // Download zip
                require 'invoice_to_zip.inc.php' ;
                break;
            case 2:
                // regenerer facture
                require  'invoice_generate.inc.php';
                break;
            case 3:
                // Envoi facture par email
                require 'invoice_send_mail_route.inc.php' ;
                break;
            case 4:
                // download all invoices in PDF
                require "invoice_download_pdf.inc.php";
        }
        return;
    }
}
$ledger_search=new Acc_Ledger_Search('VEN');
echo $ledger_search->display_search_form();
// Example
// Build the sql
list($sql, $where) = $ledger_search->build_search_sql($_GET);
// Count nb of line
$max_line = $cn->count_sql($sql);

$offset = 0;
// show a part
list($count, $html) = $ledger->list_operation($sql, $offset, 0);

// --- template Invoice  to generate --- //
$document=new ISelect('document');
$document->value=$cn->make_array("select md_id,md_name from document_modele where md_affect='VEN' order by 2");


?>
<div style="float:right"><a class="mtitle" style="font-size:140%" href="http://wiki.noalyss.eu/doku.php?id=facturation" target="_blank">Aide</a>
 </div>
<form method="GET" id="sel_sale_frm" onsubmit="return waiting_box()">
    <span class="notice"> Utiliser le bouton "Chercher" pour rechercher les factures voulues</span>
    <?php
    echo HtmlInput::request_to_hidden(array('gDossier', 'ac', 'plugin_code'));
    echo HtmlInput::request_to_hidden(array('date_start', 'date_end'));
    echo HtmlInput::request_to_hidden(array('date_paid_start', 'date_paid_end'));
    echo HtmlInput::request_to_hidden(array('amount_min', 'amount_max'));
    echo HtmlInput::request_to_hidden(array('desc', 'qcode', 'accounting'));
    echo HtmlInput::request_to_hidden(array('r_jrn'));
    echo $html;
    ?>
    <h3><?=_("Pour les factures sélectionnées")?></h3>
    <ul style="list-style-type: none">
        <li>
            
            <input type="radio" name="action" value="1" >
            <?php echo _('Télécharger les originaux') ?>
        </li>
        <li>

            <input type="radio" name="action" value="4" >
            <?php echo _('Télécharger en PDF') ?>
        </li>
        <li>
          
            <input type="radio" name="action" value="2" 
                   onclick="$('invoice_div').show();">
              <?php echo _('Générer les factures') ?>
            <div id="invoice_div" style="display:none">
                <?php echo _('Document à générer'); ?> : <?php echo $document->input(); ?>
            </div>
        </li>
        <li>
            <input type="radio" name="action" id="invoice_radio" value="3">
            <?php echo _('Envoi par email') ?>
           
        </li>
    </ul>   
    <p>
        <?php
        echo HtmlInput::submit('choice_sel', 'Exécuter');
        ?>
    </p>
</form>

    
    