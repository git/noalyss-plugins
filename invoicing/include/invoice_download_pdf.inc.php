<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Copyright Author Dany De Bontridder danydb@aevalys.eu 4/07/23
/*! 
 * \file
 * \brief  convert  selected invoice in PDF and download them
 */
if (GENERATE_PDF != 'YES') {
    echo_warning(_("Conversion en PDF non installé, ") .
        '<a href="https://wiki.noalyss.eu/doku.php?id=tutoriaux:export_pdf">Help : wiki </a>');
    return;
}

global $g_access, $g_dossier, $g_plugin_code;
$cn=Dossier::connect();

// create a tmp folder

$dirname = tempnam($_ENV['TMP'], 'invoice');
unlink($dirname);
mkdir($dirname);

$cn->start();
$feedback = array();
$a_pdf_file = array();
ini_set('mbstring.substitute_character', "none");
//--- take all the invoice
foreach ($_GET['sel_sale'] as $key => $value) {
    $a_invoice = $cn->get_array("select jr_pj_name,jr_pj from jrn where jr_id = $1", array($value));
    $invoice = $a_invoice[0];
    if ($invoice['jr_pj_name'] != "" && $invoice['jr_pj'] != "") {
        $filename = $invoice['jr_pj_name'];
        $file = $dirname . '/' . $filename;
        /*
         * Avoid that the file is overwritten by another one with
         * the same name
         */
        $i = 1;
        while (file_exists($file)) {
            $filename = sprintf("%s-%s", $i, $filename);
            $file = $dirname . '/' . $filename;
            $i++;
        }
        $cn->lo_export($invoice['jr_pj'], $file);
        $feedback[] = _('Ajout facture ') . $filename;

        //rename $filetosend : remove accentuated letter
        // remove extension
        $ext = strrpos($file, ".");
        $filetosend = substr($file, 0, $ext);
        $filetosend .= ".pdf";

        passthru(OFFICE . escapeshellarg($file), $status);

        if ($status == 1) {
            $feedback[] = "IDP71 Cannot convert to PDF $file";
            continue;
        } elseif (!file_exists($filetosend)) {
            $feedback[] = 'IDP74 ' . sprintf(_(" PDF pour %s non généré "), $file);
            continue;
        }

    }
    $a_pdf_file[] = $filetosend;


}
$cn->commit();
//  -- zip file


$date = date('ymd.Hi');
$zip_file = $_ENV['TMP'] . "/" . "invoice-" . $date . ".zip";
\Noalyss\Dbg::echo_var(1,"create ZIP file $zip_file");

// --- create the zip
$zip = new Zip_Extended();
$res = $zip->open($zip_file, ZipArchive::CREATE);
if ($res != true) {
    die("Cannot create zip file");
}
foreach ($a_pdf_file as $pdf_file)
{
     \Noalyss\Dbg::echo_var(1,"add PDF dir $dirname file $pdf_file");
    $zip->addFile( $pdf_file,basename($pdf_file));
}
$zip->close();

//-- send the zip
$link = http_build_query(array('gDossier' => $g_dossier, 'ac' => $g_access, 'plugin_code' => $g_plugin_code, 'file' => basename($zip_file)));
?>
<p>
<h2>
    <?php echo _('Facture'); ?>
</h2>
<ol>
    <?php foreach ($feedback as $row): ?>

        <li>
            <?php echo $row ?>
        </li>
    <?php endforeach; ?>
</ol>
</p>
<p>
    <a class="button" style="display:inline;"
       href="extension.raw.php?<?php echo $link; ?>"> <?php echo _('Télécharger les fichiers en PDF') ?></a>
</p>