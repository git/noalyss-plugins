<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/*!\file
 * \brief display a list of card to be paid off, let modify, remove or add
 * included from index.php
 */
require_once('am_card.class.php');

$good=new Am_Card();
/* show simply the listing, in the top, there is a button to add
 * a card, if we click on a card, we get the details and the table of
 * report
 */
$but= $good->add_card();
echo '<div class="content" >';
echo '<form method="GET">';
echo dossier::hidden();
echo HtmlInput::array_to_hidden(array('plugin_code','sa','ac'), $_REQUEST);
$ck=new ICheckBox('all');
$ck->selected=(isset ($_GET['all']))?true:false;
echo '<p> Tous les biens y compris ceux qui sont complétement amortis '.$ck->input();
echo HtmlInput::submit('look','Recherche').'</p>';
echo '</form>';
echo $but->input();
echo $good->listing($ck->selected);

echo $but->input();
echo '</div>';
?>

<div id="remaining_time_div" class="inner_box" style="top:20%;width:50rem;z-index:12;display: none;">

    <?php
    require_once "utility.class.php";
    echo HtmlInput::title_box(_("Calcul jours"),"remaining_time_div","hide");
    ?>
    <div style="padding: 1rem">

    <?php
    Noalyss\Amortiz\Utility::display_compute_time();
    ?>
    <div style="text-align: center">

        <?php
    echo \HtmlInput::button_hide("remaining_time_div");
    ?>
    </div>

    </div>

</div>
