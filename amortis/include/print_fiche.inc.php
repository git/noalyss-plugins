<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be
require_once('am_card.class.php');
/*!\file
 * \brief print detail of a card
 */
$http=new HttpInput();
echo '<div class="content" style="width:80%;margin-left:10%">';
echo '<FORM METHOD="GET">';
echo HtmlInput::hidden('sa',$http->request('sa') );
echo HtmlInput::hidden('sb',$http->request('sb'));
echo HtmlInput::hidden('ac',$http->request('ac'));
echo HtmlInput::hidden('plugin_code',$http->request('plugin_code'));
echo dossier::hidden();
$list=$cn->make_list('select fd_id from fiche_def where frd_id=7');
if ( $list !='')
  {
    $p_card=new ICard('p_card');
    $p_card->size=25;
    $p_card->set_attribute('typecard',$list);
    $p_card->set_attribute('label','p_card_label');
    $p_card->javascript=sprintf(' onchange="fill_data_onchange(\'%s\');" ',
				$p_card->name);
    $p_card->set_function('fill_data');
    $p_card->set_dblclick("fill_ipopcard(this);");
    $msg="Fiche";
    if ( isset($_GET['p_card']))
      {
	/* search the card */
	$fiche=new Fiche($cn);
        $p_card_request=$http->request('p_card');
	$fiche->get_by_qcode($p_card_request);
	$msg=$fiche->strAttribut(ATTR_DEF_NAME);
	$p_card->value=$p_card_request;
      }
    echo '<span style="text-align:left;display:block;font-size:2em" id="p_card_label"  >'.$msg.'</span>';
    echo "Fiche ".$p_card->input().$p_card->search();
    echo HtmlInput::submit('search','Accepter');
    echo '</form>';

    echo '<FORM METHOD="GET" ACTION="extension.raw.php">';
    echo HtmlInput::hidden('sa',$http->request('sa') );
    echo HtmlInput::hidden('sb',$http->request('sb'));
    echo HtmlInput::hidden('ac',$http->request('ac'));
    echo HtmlInput::hidden('plugin_code',$http->request('plugin_code'));
    echo dossier::hidden();
    echo HtmlInput::submit('pdf_all','Toutes les fiches en PDF');
    echo '</form>';

    if ( isset($_GET['search']))
      {
	$a=new Am_Card();
	echo $a->print_detail($http->request('p_card'));
      }
  }
else
  {
    echo h2info(_('Matériel à amortir'));
    echo h2(_('Attention pas de catégorie de fiche à amortir'),'class="error"');
  }
echo '</div>';