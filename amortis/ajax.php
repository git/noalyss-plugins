<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/*!\file
 * \brief ajax handling for AMORTIZING
 *@parameter $t is the target
 *@parameter $op is the request action
 *@return xml <ctl> the destination object <code> the HTML to display <extra> various usage
 */
extract ($_REQUEST, EXTR_SKIP);
$cn=Dossier::connect();
$http=new HttpInput();

$ctl=$http->request('t');
$op=$http->request('op');
$html=_('opération non trouvée');
$extra='';
$close=Icon_Action::close($ctl);
$html=$close.$html;
switch($op)
  {
    /*
     * Show a form to add new material
     */
  case 'add_mat':
    ob_start();
    require_once('include/material_add.inc.php');
    $html=ob_get_contents();
    ob_end_clean();
    break;
    /*
     * save the new material
     */
  case 'save_new_material':
    ob_start();
    require_once('include/material_new.inc.php');
    $html=ob_get_contents();
    ob_end_clean();
    break;
  case 'display_modify':
    ob_start();
    require_once('include/material_modify.inc.php');
    $html=ob_get_contents();
    ob_end_clean();
    break;

  case 'save_modify':
    ob_start();
    echo '<span id="result" style="float:left;background:red;color:white">'._("Sauvé").'</span>';

    require_once('include/material_save.inc.php');
    $f=$cn->get_value("select f_id from fiche join fiche_detail using (f_id) where ad_id=23 and ad_value=$1",array($_POST['p_card']));
    require_once('include/material_modify.inc.php');
    $html=ob_get_contents();
    ob_end_clean();
    break;
  case 'rm':
    ob_start();
    require_once('include/material_delete.inc.php');
    $html=ob_get_contents();
    ob_end_clean();
    break;
  case 'remove_concerned':
      $ha_id=$http->get("ha_id","number");
      $cn->exec_sql("update amortissement.amortissement_histo set jr_internal = null where ha_id=$1",[$ha_id]);
      $concerne=new IConcerned('op_concerne['.$ha_id.']');
      echo $concerne->input();
      return;
      break;
    case 'compute_remaining_time':

//ajax.php?gDossier=26&plugin_code=AMORTIS&ac=EXT%2FAMORTIS&t=info_prorata_div&op=compute_remaining_time&from_date=08.05.2023&to_date=08.05.2023&purchase_date=17.05.2023
        $from_date=\DateTime::createFromFormat('d.m.Y',$http->get("from_date"));
        $purchase_date=\DateTime::createFromFormat('d.m.Y',$http->get("purchase_date"));
        $to_date=\DateTime::createFromFormat('d.m.Y',$http->get("to_date"));
        if ( $from_date == false || $to_date == false || $purchase_date == false ) {
            echo _("Désolé date invalide");
            return;
        }
        // total days in exercice
        $interval_exercice=$from_date->diff($to_date);
        $total_day=$interval_exercice->format("%a")+1;

        // remaining days until end of exercice
        $interval_day=$purchase_date->diff($to_date);
        $amortiz_day=$interval_day->format("%a")+1;

        // total days in exercice
        $total_month=$interval_exercice->format("%m")+1+(($interval_exercice->format('%y'))*12);
        $amortiz_month=$interval_day->format("%m")+1+(($interval_day->format('%y'))*12);

        echo h2(_("Jours"));
        echo '<p>';
        printf (_("Nombre de jours restants %s") , $amortiz_day);
        echo '</p>';
        echo '<p>';
        printf (_("Nombre de jours exercice %s") , $total_day);
        echo '</p>';
        echo h2(_("Mois"));
        echo '<p>';
        printf (_("Nombre de mois restants %s") , $amortiz_month);
        echo '</p>';
        echo '<p>';
        printf (_("Nombre de mois exercice %s") , $total_month);
        echo '</p>';
        return;
        break;
  }
$xml=escape_xml($html);
if (headers_sent()) {
    echo $html;
} else 
{
    header('Content-type: text/xml; charset=UTF-8');
    echo '<?xml version="1.0" encoding="UTF-8"?>';
    echo '<data>';
    echo '<ctl>'.$ctl.'</ctl>';
    echo '<code>'.$xml.'</code>';
    echo '<extra>'.$extra.'</extra>';
    echo '</data>';
}