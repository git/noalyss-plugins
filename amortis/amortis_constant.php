<?php 
//This file is part of NOALYSS and is under GPL 
//see licence.txt
/**
 *@file
 *Contains all the needed variable for the plugin
 *is name is plugin_name_constant.php
 * You can use some globale variable, especially for the database
 *  connection
 */

require_once NOALYSS_INCLUDE.'/class/database.class.php';

global $cn,$amortissement_version;
global $plugin_code,$access_code;

$http=new \HttpInput();

$cn=Dossier::connect();;
$plugin_code=$http->request('plugin_code');
$access_code=$http->request('ac');
        

function detail_material($f_id,$p_label)
{
    global $plugin_code,$access_code;
  $href=sprintf('<A class="detail" style="text-decoration:underline" href="javascript:display_material(%d,%d,\'%s\',\'bxmat\',\'%s\')">%s</A>',
		dossier::id(),$f_id,$plugin_code,$access_code,$p_label);
  return $href;
}
$amortissement_version=8;

global $g_dir_amortiz;
$g_dir_amortiz=__DIR__;
?>
