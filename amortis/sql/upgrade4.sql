begin;

CREATE OR REPLACE FUNCTION amortissement.amortissement_ins()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
declare 
i int;
nyear int;
n_ad_amount numeric(20,2);
total numeric(20,2);
last_ad_id bigint;
begin
	i :=1;
	total := 0;


 	n_ad_amount := round(NEW.a_amount * NEW.a_prorata/(12*NEW.a_nb_year),2);
            
	nyear := NEW.a_start;
	total := n_ad_amount;

	insert into amortissement.amortissement_detail(ad_year,ad_amount,a_id,ad_percentage) values (nyear,n_ad_amount,NEW.a_id,round(1/NEW.a_nb_year,4)) returning ad_id into last_ad_id;
    insert into amortissement.amortissement_histo(a_id,h_amount,h_year) values (NEW.a_id,0,nyear);
	
	loop
	   
	   if i = NEW.a_nb_year+1  then
		exit ;
	   end if;
       
	   nyear :=  NEW.a_start +i;

	   n_ad_amount := round(NEW.a_amount/NEW.a_nb_year,2);

       total := total + n_ad_amount;

       if total > NEW.a_amount then
			n_ad_amount := n_ad_amount- total + NEW.a_amount ;
	   end if;

       insert into amortissement.amortissement_detail(ad_year,ad_amount,a_id,ad_percentage) values (nyear,n_ad_amount,NEW.a_id,round(1/NEW.a_nb_year,4)) returning ad_id into last_ad_id;
       insert into amortissement.amortissement_histo(a_id,h_amount,h_year) values (NEW.a_id,0,nyear);
	   i := i+1;
	end loop;
	if total < NEW.a_amount then
		n_ad_amount := n_ad_amount+NEW.a_amount-total;
		update amortissement.amortissement_detail set ad_amount=n_ad_amount where ad_id=last_ad_id;
	end if;
	return NEW;
end;


$function$;

insert into amortissement.version values (5);
commit;
