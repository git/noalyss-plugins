<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USAtv
*/
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be
global $version_plugin;
$version_plugin=SVNINFO;
Extension::check_version(9000);
$http=new HttpInput();
$ac=$http->request("ac");
$plugin_code=$http->request("plugin_code")
?>
<script>
    const ac = '<?php echo $ac?>';
    const plugin_code = '<?php echo $plugin_code; ?>';
    const dossier_id = '<?php echo Dossier::id();?>';
</script>
<?php
/*!\file
 * \brief main entry
 */
require_once __DIR__.'/amortis_constant.php';
require_once NOALYSS_INCLUDE.'/class/database.class.php';
require_once NOALYSS_INCLUDE.'/class/dossier.class.php';

global $amortissement_version;
/*
 * load javascript
 */
ob_start();
require_once('amortize_javascript.js');
$j=ob_get_contents();
ob_end_clean();
echo create_script($j);
?>
<script>
content[400]="<?=_("Filtrer sur quick-code, nom, date d'acquisition ou Année d'achat");?>";
content[401]="<?=_("Nombre de mois ou jour sur la première année")?>";

</script>
<?php
$url="?".http_build_query(array(
        "gDossier"=>Dossier::id(),
        "plugin_code"=>$plugin_code,
        "ac"=>$ac
    ));
$cn=Dossier::connect();

if ( $cn->exist_schema('amortissement') ==false )
  {
    require_once __DIR__.'/include/install_plugin.class.php';
    $plugin=new \amortiz\Install_Plugin($cn);
    $plugin->install();
  }
 
// Upgrade plugin if needed
$current=$cn->get_value('select max(val) from amortissement.version') ;
if ( $current < $amortissement_version )
{
        require_once('include/install_plugin.class.php');
	$iplugn = new \amortiz\Install_Plugin($cn);
        for ( $e = $current+1;$e <= $amortissement_version ; $e++)
        {
            $iplugn->upgrade($e);
        }
}

$menu=array(
        array($url.'&sa=card',_('Biens amortissables'),_('Liste des biens amortissables'),1),
        array($url.'&sa=report',_('Rapport'),_('rapport et  tableaux sur les biens amortissables'),2),
        array($url.'&sa=util',_('Utilitaire'),_('Génération écriture comptable'),3)
      );


$sa=$http->request('sa',"string","card");

$def=0;

switch($sa)
  {
  case 'card':
    $def=1;
    break;
  case 'report':
    $def=2;
    break;
  case 'util':
    $def=3;
    break;
  }
  echo '<div style="float:right" class="nav-level2  nav-pills"><a class="nav-link" style="display:inline" href="http://wiki.noalyss.eu/doku.php?id=amortissement" target="_blank">'.
        _("Aide").
        '</a>'.
'<span style="font-size:0.8em;color:red;display:inline">v:SVNINFO</span>'.
'</div>';

echo '<div class="topmenu">';
echo ShowItem($menu,'H','nav-item ','nav-link',$def,'nav nav-pills nav-level2');
echo '</div>';

/* List + add and modify card */
if ($def==1)
  {
    require_once('include/am_card.php');
    exit();
  }
/* report */
if ( $def==2)
  {
    require_once('include/am_print.php');
    exit();
  }
/* Utility : write in accountancy */
if ( $def==3)
  {
    require_once('include/am_util.php');
    exit();
  }
