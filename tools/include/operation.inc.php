<?php
//This file is part of NOALYSS and is under GPL 
//see licence.txt
require_once NOALYSS_INCLUDE."/class/acc_ledger_search.class.php";

$ledger=new Acc_Ledger_Search("ALL");
$search_box=$ledger->search_form();
echo '<div class="content">';

echo '<form method="GET">';
echo $search_box;
echo HtmlInput::submit("viewsearch",_("Recherche"));
echo HtmlInput::extension();
echo HtmlInput::hidden('sa',$_REQUEST['sa']);
echo HtmlInput::hidden('ac',$_REQUEST['ac']);
echo '</form>';

/*
 * Change accounting
 */
if (isset($_POST['chg_poste']))
  {
       change_accounting($cn);
  }
/*
 * Change card
 */
if (isset($_POST['chg_card']))
  {
    change_card($cn);
  }
/*
 * Change ledger
 */
if (isset($_POST['chg_ledger']))
  {
    change_ledger($cn);
  }
/*
 * Change ledger
 */
if (isset($_POST['chg_card_account']))
  {
    change_card_account($cn);
  }
/**
 * change analytic
 */  
 if ( isset($_POST['chg_anlt'])) {
     change_analytic($cn);
 }
/**
 * Set , remove clean tags
 */
if (isset($_POST['tag_set']))
{
    tools_set_tag();

}
//-----------------------------------------------------
// Display search result
//-----------------------------------------------------
if ( isset ($_GET['viewsearch']))
{

    if (count ($_GET) == 0)
        $array=null;
    else
        $array=$_GET;
    $array['p_action']='ALL';
    list($sql,$where)=$ledger->build_search_sql($array);

    // order
    $sql.=' order by jr_date_order asc,substring(jr_pj_number,\'[0-9]+$\')::numeric asc  ';

    // Count nb of line
    $max_line=$cn->count_sql($sql);
echo HtmlInput::button('accounting_bt','Changer poste comptable','onclick="Tools.toggle(\'div_poste\')"');

echo HtmlInput::button('card_bt','Changer fiche','onclick="Tools.toggle(\'div_card\')"');

echo HtmlInput::button('ledger_bt','Déplacement dans Journal','onclick="Tools.toggle(\'div_ledger\')"');
echo HtmlInput::button('card_acc_bt','Poste > fiche','onclick="Tools.toggle(\'div_card_account\')"');
echo HtmlInput::button("anlt_bt","Modifie Analytique",'onclick="Tools.toggle(\'anlt_div\')"');
echo HtmlInput::button("tag_blt","Etiquette",'onclick="Tools.toggle(\'div_tag\')"');
require_once 'template/search_view.php';

}

echo '</div>';


?>